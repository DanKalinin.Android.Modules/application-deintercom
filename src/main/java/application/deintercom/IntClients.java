package application.deintercom;

import java.util.ArrayList;
import library.deintercom.IntClient;
import library.glibext.GeException;

public class IntClients extends ArrayList<IntClient> {
    public interface AnswerAsyncCallback {
        void callback(IntClient client, GeException exception);
    }

    public void answerAsync(byte a, AnswerAsyncCallback callback) {
        boolean[] success = {false};
        int[] exceptions = {0};

        for (IntClient client : this) {
            client.answerAsync(a, exception -> {
                if (exception == null) {
                    if (success[0]) return;
                    callback.callback(client, null);
                    success[0] = true;
                } else {
                    exceptions[0]++;
                    if (exceptions[0] < size()) return;
                    callback.callback(null, exception);
                }
            });
        }
    }
}
