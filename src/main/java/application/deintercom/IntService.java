package application.deintercom;

import android.app.Notification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import library.android.app.LaaService;
import library.deintercom.IntClient;
import library.deintercom.IntDatabase;
import library.deintercom.IntUsbSerial;
import library.glibext.GeException;

public class IntService extends LaaService implements IntContext.Listener {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        IntContext.shared().listeners.add(this);
        IntContext.shared().service = this;

        Notification notification = new NotificationCompat.Builder(this, IntContext.CHANNEL_INTERCOM)
                .setSmallIcon(R.drawable.phone)
                .setContentTitle("Hello")
                .setContentText("World")
                .setSilent(true)
                .build();

        startForeground(IntContext.NOTIFICATION_SERVICE, notification);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        IntContext.shared().listeners.remove(this);
    }

    @Override
    public void call(IntContext sender, IntUsbSerial serial, byte a) {
        try {
            if (a == 1) return;
            sender.agent.callHangupIncoming();
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void commit(IntContext sender, IntDatabase database) {

    }

    @Override
    public void call(IntContext sender, IntClient client, byte a) {
        sender.call(client.password, a);
    }

    @Override
    public void ip(IntContext sender, IntClient client, String local, String external) {

    }

    @Override
    public void accept(IntContext sender, IntClient client, String id, byte a) {
        sender.accept(client.password, id, a);
    }

    @Override
    public void progress(IntContext sender, IntClient client, long current, long total, int tag) {

    }

    @Override
    public void preferences(IntContext sender, SharedPreferences preferences, String key) {

    }
}
