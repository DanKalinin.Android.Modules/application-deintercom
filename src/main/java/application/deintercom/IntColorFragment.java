package application.deintercom;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.navigation.NavigationView;
import application.deintercom.databinding.IntColorFragmentBinding;
import library.com.google.android.material.bottomsheet.LcgambBottomSheetDialogFragment;

public class IntColorFragment extends LcgambBottomSheetDialogFragment implements NavigationView.OnNavigationItemSelectedListener {
    public IntColorFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding != null) return binding.getRoot();

        binding = IntColorFragmentBinding.inflate(inflater, container, false);
        binding.navigationView.setNavigationItemSelectedListener(this);
        binding.navigationView.setItemIconTintList(null);
        getColor();
        return binding.getRoot();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        String color = IntColor.idToColor(item.getItemId());
        SharedPreferences.Editor editor = IntContext.shared().preferences.edit();
        editor.putString(IntContext.KEY_COLOR, color);
        editor.apply();
        dismiss();
        requireActivity().recreate();
        return true;
    }

    public void getColor() {
        String color = IntContext.shared().preferences.getString(IntContext.KEY_COLOR, null);
        int id = IntColor.colorToId(color);
        MenuItem item = binding.navigationView.getMenu().findItem(id);
        item.setVisible(false);
    }
}
