package application.deintercom;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import library.android.app.admin.LaaaDeviceAdminReceiver;

public class IntAdminReceiver extends LaaaDeviceAdminReceiver {
    @Override
    public void onEnabled(Context context, Intent intent) {
        DevicePolicyManager manager = getManager(context);
        ComponentName admin = getWho(context);
        ArrayList<String> packages = new ArrayList<>();
        packages.add(context.getPackageName());
        manager.setLockTaskPackages(admin, packages.toArray(new String[0]));
        manager.setLockTaskFeatures(admin, DevicePolicyManager.LOCK_TASK_FEATURE_SYSTEM_INFO);

        Intent intent1 = new Intent(context, IntSplashActivity.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent1);
    }
}
