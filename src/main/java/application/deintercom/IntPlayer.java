package application.deintercom;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import java.io.IOException;
import java.util.ArrayList;
import library.android.media.LamMediaPlayer;

public class IntPlayer extends LamMediaPlayer implements MediaPlayer.OnCompletionListener, Runnable {
    public interface Listener {
        void path(IntPlayer sender);
        void playing(IntPlayer sender);
        void position(IntPlayer sender);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void path(IntPlayer sender) {
            for (Listener listener : this) {
                listener.path(sender);
            }
        }

        @Override
        public void playing(IntPlayer sender) {
            for (Listener listener : this) {
                listener.playing(sender);
            }
        }

        @Override
        public void position(IntPlayer sender) {
            for (Listener listener : this) {
                listener.position(sender);
            }
        }
    }

    private static IntPlayer ret;
    public Listeners listeners;
    public Handler handler;
    public String path;

    @Override
    public void setDataSource(String path) throws IOException, IllegalArgumentException, IllegalStateException, SecurityException {
        if (path.equals(this.path)) return;
        super.setDataSource(path);
        this.path = path;
        listeners.path(this);
    }

    @Override
    public void start() throws IllegalStateException {
        if (isPlaying()) return;
        super.start();
        listeners.playing(this);
        handler.removeCallbacks(this);
        handler.postDelayed(this, 500);
    }

    @Override
    public void pause() throws IllegalStateException {
        if (!isPlaying()) return;
        super.pause();
        listeners.playing(this);
    }

    @Override
    public void seekTo(int msec) throws IllegalStateException {
        if (msec == getCurrentPosition()) return;
        super.seekTo(msec);
        listeners.position(this);
    }

    @Override
    public void reset() {
        if (path == null) return;
        super.reset();
        path = null;
        listeners.path(this);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        listeners.playing(this);
        seekTo(0);
    }

    @Override
    public void run() {
        if (!isPlaying()) return;
        listeners.position(this);
        handler.postDelayed(this, 500);
    }

    public static IntPlayer shared() {
        if (ret != null) return ret;
        ret = new IntPlayer();
        ret.setOnCompletionListener(ret);
        ret.listeners = new Listeners();
        ret.handler = new Handler(Looper.getMainLooper());
        return ret;
    }
}
