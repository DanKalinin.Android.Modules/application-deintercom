package application.deintercom;

import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Looper;
import java.io.File;
import library.fdroidext.FdrApplication;
import library.fdroidext.FdrClient;
import library.fdroidext.FdrIndex;
import library.fdroidext.FdrPackage;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntFdroid extends LjlObject {
    private static IntFdroid ret;
    public FdrClient client;

    public void abort() {
        client.cancel(hashCode());
    }

    public boolean delete() {
        String name = BuildConfig.APPLICATION_ID + ".apk";
        File file = new File(IntContext.shared().application.getFilesDir(), name);
        if (!file.exists()) return true;
        PackageInfo info = IntContext.shared().application.getPackageManager().getPackageArchiveInfo(file.toString(), 0);
        if (info.versionCode > BuildConfig.VERSION_CODE) return true;
        return file.delete();
    }

    public static IntFdroid shared() {
        if (ret != null) return ret;
        ret = new IntFdroid();
        ret.client = FdrClient.client("https://main-dankalinin-gitlab-pages-681cf87dff66e2f5294514f7b81b754af3.gitlab.io/en/fdroid/repo/", "bca9120186a1cc1c7a97f73984c44c1ca26c9c7c835c0ca8eb856b8dc54f05e2");
        return ret;
    }

    // Index

    public FdrIndex indexSync() throws GeException {
        FdrIndex ret = null;
        String name = BuildConfig.APPLICATION_ID + ".apk";
        File file = new File(IntContext.shared().application.getFilesDir(), name);

        if (file.exists()) {
            PackageInfo info = IntContext.shared().application.getPackageManager().getPackageArchiveInfo(file.toString(), 0);
            ret = new FdrIndex();
            ret.applications = new FdrApplication[1];
            ret.applications[0] = new FdrApplication();
            ret.applications[0].id = BuildConfig.APPLICATION_ID;
            ret.applications[0].name = (String) info.applicationInfo.loadLabel(IntContext.shared().application.getPackageManager());
            ret.applications[0].desc = (String) info.applicationInfo.loadDescription(IntContext.shared().application.getPackageManager());
            ret.applications[0].packages = new FdrPackage[1];
            ret.applications[0].packages[0] = new FdrPackage();
            ret.applications[0].packages[0].version = info.versionName;
            ret.applications[0].packages[0].versioncode = info.versionCode;
            ret.applications[0].packages[0].apkname = name;
        } else {
            String base = IntContext.shared().application.getString(R.string.fdroid_base);
            String fingerprint = IntContext.shared().application.getString(R.string.fdroid_fingerprint);
            FdrClient client = FdrClient.client(base, fingerprint);
            ret = client.indexSync();
        }

        return ret;
    }

    public interface IndexAsyncCallback {
        void callback(FdrIndex index, GeException exception);
    }

    public void indexAsync(IndexAsyncCallback callback) {
        new Thread(() -> {
            try {
                FdrIndex index = indexSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(index, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Download

    public File downloadSync(String name) throws GeException {
        File ret = new File(IntContext.shared().application.getFilesDir(), name);
        if (ret.exists()) return ret;
        client.downloadSync(name, ret.toString(), hashCode());
        return ret;
    }

    public interface DownloadAsyncCallback {
        void callback(File file, GeException exception);
    }

    public void downloadAsync(String name, DownloadAsyncCallback callback) {
        new Thread(() -> {
            try {
                File file = downloadSync(name);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(file, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Delete

    public boolean deleteSync() {
        for (int code = 1; code <= BuildConfig.VERSION_CODE; code++) {
            String name = BuildConfig.APPLICATION_ID + "_" + code + ".apk";
            File file = new File(IntContext.shared().application.getFilesDir(), name);
            if (!file.exists()) continue;
            if (!file.delete()) return false;
        }

        return true;
    }

    public interface DeleteAsyncCallback {
        void callback(boolean deleted);
    }

    public void deleteAsync(DeleteAsyncCallback callback) {
        new Thread(() -> {
            boolean deleted = deleteSync();
            if (callback == null) return;
            new Handler(Looper.getMainLooper()).post(() -> callback.callback(deleted));
        }).start();
    }
}
