package application.deintercom;

import library.deintercom.IntBackend;
import library.deintercom.IntClient;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntCalls extends LjlObject implements Runnable {
    public String id;

    @Override
    public void run() {
        try {
            IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(id);
            if (backends.length == 0) return;
            IntBackend backend = backends[0];
            String baseLocal = "https://" + backend.local + ":60000";
            IntClient local = IntClient.client(baseLocal, IntCredentials.shared().id, backend.id, IntContext.shared().databaseClient);
            local.callsV1Async(null);
            local.filesV1Async("music", null);
            if (backend.external == null) return;
            String baseExternal = "https://" + backend.external + ":60000";
            IntClient external = IntClient.client(baseExternal, IntCredentials.shared().id, backend.id, IntContext.shared().databaseClient);
            external.callsV1Async(null);
            external.filesV1Async("music", null);
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    public static IntCalls newInstance(String id) {
        IntCalls ret = new IntCalls();
        ret.id = id;
        return ret;
    }
}
