package application.deintercom;

import android.Manifest;
import android.app.NotificationManager;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.WindowManager;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import application.deintercom.databinding.IntCallActivityBinding;
import library.deintercom.IntBackend;
import library.deintercom.IntClient;
import library.deintercom.IntPinger;
import library.glibext.GeException;

public class IntCallActivity extends IntActivity implements View.OnClickListener, ActivityResultCallback<Boolean>, Runnable {
    public IntCallActivityBinding binding;
    public ActivityResultLauncher<String> launcher;
    public Ringtone ringtone;
    public IntPinger pinger;
    public int account = -1;
    public int call = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = IntCallActivityBinding.inflate(getLayoutInflater());
        binding.buttonDecline.setOnClickListener(this);
        binding.buttonOpen.setOnClickListener(this);
        binding.buttonClose.setOnClickListener(this);
        binding.buttonAccept.setOnClickListener(this);
        binding.buttonEnd.setOnClickListener(this);
        binding.buttonMuteOn.setOnClickListener(this);
        binding.buttonMuteOff.setOnClickListener(this);
        binding.buttonSpeakerOn.setOnClickListener(this);
        binding.buttonSpeakerOff.setOnClickListener(this);
        setContentView(binding.getRoot());

        launcher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), this);

        IntContext.shared().callActivity = this;
        IntContext.shared().callId = getIntent().getStringExtra("id");

        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.cancel(IntContext.NOTIFICATION_CALL);

        backendSelectById();

        binding.getRoot().postDelayed(this, 30000);

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtone = RingtoneManager.getRingtone(this, uri);
        ringtone.play();

        if (getIntent().getAction().equals(IntContext.ACTION_ACCEPT)) onClick(binding.buttonAccept);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            binding.getRoot().removeCallbacks(this);
            if (ringtone != null) ringtone.stop();
            if (pinger != null) pinger.close();
            if (call != -1) IntContext.shared().agent.callHangup(call);
            if (account != -1) IntContext.shared().agent.accountDelete(account);

            IntContext.shared().callActivity = null;
            IntContext.shared().callId = null;

            AudioManager manager = getSystemService(AudioManager.class);
            manager.setMicrophoneMute(false);
            manager.setSpeakerphoneOn(false);
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        try {
            if (v.equals(binding.buttonDecline)) {
                finish();
            } else if (v.equals(binding.buttonOpen)) {
                loading(true);
                pinger.current.openAsync((byte) 1, exception -> {
                    loading(false);

                    if (exception == null) {
                        binding.layoutOpen.setVisibility(View.GONE);
                        binding.layoutClose.setVisibility(View.VISIBLE);
                    } else {
                        exception(getString(R.string.unable_to_reach_intercom));
                    }
                });
            } else if (v.equals(binding.buttonClose)) {
                loading(true);
                pinger.current.openAsync((byte) 0, exception -> {
                    loading(false);

                    if (exception == null) {
                        binding.layoutOpen.setVisibility(View.VISIBLE);
                        binding.layoutClose.setVisibility(View.GONE);
                    } else {
                        exception(getString(R.string.unable_to_reach_intercom));
                    }
                });
            } else if (v.equals(binding.buttonAccept)) {
                binding.getRoot().removeCallbacks(this);

                if (ringtone != null) ringtone.stop();
                ringtone = null;

                IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(IntContext.shared().callId);
                if (backends.length == 0) return;
                IntBackend backend = backends[0];

                IntClients clients = new IntClients();
                String baseLocal = "https://" + backend.local + ":60000";
                IntClient local = IntClient.client(baseLocal, IntCredentials.shared().id, backend.id, null);
                clients.add(local);
                String baseExternal = backend.external == null ? null : "https://" + backend.external + ":60000";
                IntClient external = backend.external == null ? null : IntClient.client(baseExternal, IntCredentials.shared().id, backend.id, null);
                if (backend.external != null) clients.add(external);

                loading(true);
                clients.answerAsync((byte) 1, (client, exception) -> {
                    loading(false);

                    if (client == null) {
                        exception(getString(R.string.unable_to_reach_intercom));
                    } else {
                        pinger = IntPinger.pinger(local, external, client);

                        binding.textViewDetail.setVisibility(View.GONE);
                        binding.chronometer.setBase(SystemClock.elapsedRealtime());
                        binding.chronometer.start();
                        binding.chronometer.setVisibility(View.VISIBLE);
                        binding.layoutDecline.setVisibility(View.GONE);
                        binding.layoutOpen.setVisibility(View.VISIBLE);
                        binding.layoutAccept.setVisibility(View.GONE);
                        binding.layoutEnd.setVisibility(View.VISIBLE);
                        binding.layoutMuteOn.setVisibility(View.VISIBLE);
                        binding.layoutSpeakerOn.setVisibility(View.VISIBLE);

                        launcher.launch(Manifest.permission.RECORD_AUDIO);
                    }
                });
            } else if (v.equals(binding.buttonEnd)) {
                pinger.current.answerAsync((byte) 0, null);
                finish();
            } else if (v.equals(binding.buttonMuteOn)) {
                binding.layoutMuteOn.setVisibility(View.GONE);
                binding.layoutMuteOff.setVisibility(View.VISIBLE);

                AudioManager manager = getSystemService(AudioManager.class);
                manager.setMicrophoneMute(true);
            } else if (v.equals(binding.buttonMuteOff)) {
                binding.layoutMuteOn.setVisibility(View.VISIBLE);
                binding.layoutMuteOff.setVisibility(View.GONE);

                AudioManager manager = getSystemService(AudioManager.class);
                manager.setMicrophoneMute(false);
            } else if (v.equals(binding.buttonSpeakerOn)) {
                binding.layoutSpeakerOn.setVisibility(View.GONE);
                binding.layoutSpeakerOff.setVisibility(View.VISIBLE);

                AudioManager manager = getSystemService(AudioManager.class);
                manager.setSpeakerphoneOn(true);
            } else if (v.equals(binding.buttonSpeakerOff)) {
                binding.layoutSpeakerOn.setVisibility(View.VISIBLE);
                binding.layoutSpeakerOff.setVisibility(View.GONE);

                AudioManager manager = getSystemService(AudioManager.class);
                manager.setSpeakerphoneOn(false);
            }
        } catch (GeException exception) {
            exception(getString(R.string.unknown_error));
        }
    }

    @Override
    public void onActivityResult(Boolean result) {
        try {
            if (result) {
                String host = pinger.current.base.substring(8, pinger.current.base.length() - 6);
                boolean stun = pinger.current == pinger.external;
                account = IntContext.shared().agent.accountAdd(host, IntCredentials.shared().id, pinger.current.password, stun);
                call = IntContext.shared().agent.callMake(account, host, "6000");
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                IntRationaleFragment fragment = IntRationaleFragment.newInstance(launcher, Manifest.permission.RECORD_AUDIO);
                fragment.show(getSupportFragmentManager(), null);
            } else {
                IntDeniedFragment fragment = IntDeniedFragment.newInstance(Manifest.permission.RECORD_AUDIO);
                fragment.show(getSupportFragmentManager(), null);
            }
        } catch (GeException exception) {
            exception(getString(R.string.unknown_error));
        }
    }

    @Override
    public void run() {
        finish();
    }

    public void loading(boolean loading) {
        binding.progressIndicator.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        if (loading) {
            binding.progressIndicator.setVisibility(View.VISIBLE);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public void exception(String text) {
        IntErrorFragment fragment = IntErrorFragment.newInstance(text);
        fragment.show(getSupportFragmentManager(), null);
    }

    public void backendSelectById() {
        try {
            IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(IntContext.shared().callId);
            if (backends.length == 0) return;
            IntBackend backend = backends[0];
            String text = (backend.name == null) ? getString(R.string.intercom) : backend.name;
            binding.textViewCaller.setText(text);
        } catch (GeException exception) {
            exception(getString(R.string.unknown_error));
        }
    }
}
