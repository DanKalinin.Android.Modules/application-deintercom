package application.deintercom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import java.util.ArrayDeque;
import java.util.Iterator;
import library.android.view.LavView;

public class IntWaveView extends LavView {
    public float thickness;
    public float spacing;
    public int color;
    public Paint paint;
    public RectF frame;
    public ArrayDeque<Float> amplitudes;

    public IntWaveView(Context context, AttributeSet attrs) {
        super(context, attrs);

        thickness = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.0f, getResources().getDisplayMetrics());
        spacing = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.0f, getResources().getDisplayMetrics());
        color = context.getColor(R.color.black);

        TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.IntWaveView, 0, 0);
        thickness = attributes.getDimension(R.styleable.IntWaveView_thickness, thickness);
        spacing = attributes.getDimension(R.styleable.IntWaveView_spacing, spacing);
        color = attributes.getColor(R.styleable.IntWaveView_color, color);
        attributes.recycle();

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(thickness);

        amplitudes = new ArrayDeque<>();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        float right = w - getPaddingRight();
        float bottom = h - getPaddingBottom();
        frame = new RectF(getPaddingLeft(), getPaddingTop(), right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Iterator<Float> iterator = amplitudes.descendingIterator();
        float dx = thickness + spacing;
        float x0 = 0.5f * (frame.width() % dx + dx);
        float x = frame.width() - x0;
        float k = 0.5f * (frame.height() - thickness);

        while (iterator.hasNext()) {
            float amplitude = iterator.next();
            float dy = k * amplitude;
            float startY = frame.centerY() + dy;
            float stopY = frame.centerY() - dy;
            canvas.drawLine(x, startY, x, stopY, paint);
            x -= dx;
            if (x < x0) break;
        }
    }

    public void add(float amplitude) {
        amplitudes.add(amplitude);
        invalidate();
    }

    public void clear() {
        amplitudes.clear();
        invalidate();
    }
}
