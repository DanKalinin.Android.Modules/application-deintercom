package application.deintercom;

import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.io.File;
import java.util.ArrayList;
import application.deintercom.databinding.IntPasswordFragmentBinding;
import library.deintercom.IntAction;
import library.deintercom.IntAsrRecognizer;
import library.deintercom.IntFFmpeg;
import library.deintercom.IntFile;
import library.deintercom.IntMessage;
import library.deintercom.IntText;
import library.glibext.GeException;
import library.voskext.VskResult;
import library.voskext.VskWord;

public class IntPasswordFragment extends IntFragment implements ActivityResultCallback<Boolean>, View.OnClickListener, IntChatAdapter.Listener, IntChatView.Listener, IntRecord.Listener {
    public IntPasswordFragmentBinding binding;
    public ActivityResultLauncher<String> launcher;
    public ArrayList<IntMessage> messages;
    public int n;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding != null) return binding.getRoot();

        launcher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), this);

        messages = new ArrayList<>();
        IntChatAdapter adapter = IntChatAdapter.newInstance(messages.toArray(new IntMessage[0]), false, false);
        adapter.listener = this;
        LinearLayoutManager manager = new LinearLayoutManager(getContext());

        binding = IntPasswordFragmentBinding.inflate(inflater, container, false);
        binding.toolbar.setNavigationOnClickListener(this);
        binding.chatLayout.binding.recyclerView.setAdapter(adapter);
        binding.chatLayout.binding.recyclerView.setLayoutManager(manager);
        binding.error.buttonRetry.setOnClickListener(this);
        binding.chatView.listeners.add(this);
        //
        addRecord();
        //
        showChatView();
        return binding.getRoot();

        /*
        IntContext.shared().listeners.add(this);

        IntMessage[] messages = {};
        IntChatAdapter adapter = IntChatAdapter.newInstance(messages);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());

        binding = IntChatFragmentBinding.inflate(inflater, container, false);
        binding.toolbar.setOnMenuItemClickListener(this);
        binding.toolbar.setNavigationOnClickListener(this);
        binding.chatLayout.binding.recyclerView.setAdapter(adapter);
        binding.chatLayout.binding.recyclerView.setLayoutManager(manager);
        binding.noBackend.buttonAdd.setOnClickListener(this);
        backendSelectById();
        binding.chatLayout.scrollToEnd();
        return binding.getRoot();
         */
    }

    @Override
    public void onPop() {
        hideChatView();
    }

    @Override
    public void onActivityResult(Boolean result) {
        if (result) {
            binding.chatView.record();
            IntRecord.shared().listeners.remove(this);
            IntRecord.shared().listeners.add(this);
            IntRecord.shared().startRecording();
        } else if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
            IntRationaleFragment fragment = IntRationaleFragment.newInstance(launcher, Manifest.permission.RECORD_AUDIO);
            fragment.show(getChildFragmentManager(), null);
        } else {
            IntDeniedFragment fragment = IntDeniedFragment.newInstance(Manifest.permission.RECORD_AUDIO);
            fragment.show(getChildFragmentManager(), null);
        }
    }

    @Override
    public void onClick(View v) {
        IntNavigation.shared().pop(this);
    }

    @Override
    public void action(IntChatAdapter sender, IntMessage message, IntAction action) {
        passwordDelete();
//        Toast.makeText(getContext(), action.text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRecord(IntChatView sender) {
        launcher.launch(Manifest.permission.RECORD_AUDIO);
    }

    @Override
    public void onSend(IntChatView sender) {
        IntRecord.shared().stop();
        send();
    }

    @Override
    public void onCancel(IntChatView sender) {
        IntRecord.shared().stop();
    }

    @Override
    public void onStop(IntRecord sender) {
        binding.chatView.stop();
        send();
    }

    public void recognizeAsyncCallback(VskResult result, GeException exception) {
        if ((result == null) || (result.result.length == 0)) {
            addError();
            binding.chatLayout.smoothScrollToEnd();
        } else {
            VskWord firstWord = result.result[0];
            VskWord lastWord = result.result[result.result.length - 1];
            String child = IntCredentials.shared().id + "/password/_password.m4a";
            File output = new File(requireContext().getFilesDir(), child);
            IntFFmpeg.convertPcmDataAsync(IntRecord.shared().audioData, n, output.toString(), IntRecord.shared().getSampleRate(), firstWord.start, lastWord.end, result.text, this::convertPcmDataAsyncCallback);
        }
    }

    public void convertPcmDataAsyncCallback(GeException exception) {
        if (exception == null) {
            passwordUpload();
//            IntPlayer.shared().reset();
//            addFile();
        } else {
            Toast.makeText(getContext(), exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

//    public void messages() {
//        record = new IntMessage();
//        record.timestamp = -1;
//        record.text = new IntText();
//        record.text.body = "Запишите голосовой пароль";
//
//        success = new IntMessage();
//        success.timestamp = -1;
//        success.text = new IntText();
//        success.text.body = "Голосовой пароль установлен";
//
//        error = new IntMessage();
//        error.timestamp = -1;
//        error.text = new IntText();
//        error.text.body = "Речь не распознана. Попробуйте еще раз";
//
//        current = new IntMessage();
//        current.timestamp = -1;
//        current.text = new IntText();
//        current.text.body = "Ваш голосовой пароль";
//
//        password = new IntMessage();
//        password.timestamp = -1;
//        password.right = true;
//        password.file = new IntFile();
//        password.file.backend = IntCredentials.shared().id;
//        password.file.directory = ".";
//        password.file.name = "password.m4a";
//        password.file.duration = 2.0;
//    }

    public void showChatView() {
        binding.chatView.setVisibility(View.VISIBLE);
        ((IntMainActivity) requireActivity()).binding.bottomNavigationView.setVisibility(View.GONE);
    }

    public void hideChatView() {
        binding.chatView.setVisibility(View.GONE);
        ((IntMainActivity) requireActivity()).binding.bottomNavigationView.setVisibility(View.VISIBLE);
    }

    public void send() {
        try {
            n = IntRecord.shared().read(IntRecord.shared().audioData, 0, IntRecord.shared().audioData.length, IntRecord.READ_NON_BLOCKING);
            IntAsrRecognizer recognizer = IntAsrRecognizer.recognizer(IntContext.shared().model, IntRecord.shared().getSampleRate());
            recognizer.recognizeAsync(IntRecord.shared().audioData, n, this::recognizeAsyncCallback);
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    public void addRecord() {
        IntMessage message = new IntMessage();
        message.timestamp = System.currentTimeMillis();
        message.text = new IntText();
        message.text.body = "Запишите голосовой пароль";
        messages.add(message);

        IntMessage message3 = new IntMessage();
        message3.right = true;
        message3.timestamp = message.timestamp + 1;
        message3.actions = new IntAction[2];
        message3.actions[0] = new IntAction();
        message3.actions[0].id = 1;
        message3.actions[0].text = "Change";
        message3.actions[1] = new IntAction();
        message3.actions[1].id = 2;
        message3.actions[1].text = "Delete";
        messages.add(message3);

        ((IntChatAdapter) binding.chatLayout.binding.recyclerView.getAdapter()).setMessages(messages.toArray(new IntMessage[0]));
    }

    public void addError() {
        IntMessage message = new IntMessage();
        message.timestamp = System.currentTimeMillis();
        message.text = new IntText();
        message.text.body = "Речь не распознана. Попробуйте еще раз";
        messages.add(message);
        ((IntChatAdapter) binding.chatLayout.binding.recyclerView.getAdapter()).setMessages(messages.toArray(new IntMessage[0]));
    }

    public void addFile() {
        try {
            String child = IntCredentials.shared().id + "/password/password.m4a";
            File file = new File(requireContext().getFilesDir(), child);

            IntMessage message = new IntMessage();
            message.right = true;
            message.timestamp = System.currentTimeMillis();
            message.file = new IntFile();
            message.file.backend = IntCredentials.shared().id;
            message.file.directory = "password";
            message.file.name = "password.m4a";
            message.file.duration = IntFFmpeg.duration(file.toString());
            messages.add(message);

            IntMessage message1 = new IntMessage();
            message1.right = true;
            message1.timestamp = message.timestamp + 1;
            message1.text = new IntText();
            message1.text.body = IntFFmpeg.lyrics(file.toString());
            messages.add(message1);

            IntMessage message2 = new IntMessage();
            message2.timestamp = message1.timestamp + 1;
            message2.text = new IntText();
            message2.text.body = "Голосовой пароль установлен";
            messages.add(message2);

            IntMessage message3 = new IntMessage();
            message3.right = true;
            message3.timestamp = message2.timestamp + 1;
            message3.actions = new IntAction[2];
            message3.actions[0] = new IntAction();
            message3.actions[0].id = 1;
            message3.actions[0].text = "Change";
            message3.actions[1] = new IntAction();
            message3.actions[1].id = 2;
            message3.actions[1].text = "Delete";
            messages.add(message3);

            ((IntChatAdapter) binding.chatLayout.binding.recyclerView.getAdapter()).setMessages(messages.toArray(new IntMessage[0]));
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    public void passwordUpload() {
        String child = IntCredentials.shared().id + "/password/_password.m4a";
        File file = new File(requireContext().getFilesDir(), child);

        IntContext.shared().pinger.current.passwordUploadAsync("password.m4a", file.toString(), hashCode(), (password, exception) -> {
            if (password == null) {
                Toast.makeText(getContext(), exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "OK", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void passwordDelete() {
        IntContext.shared().pinger.current.passwordDeleteAsync("password.m4a", exception -> {
            if (exception == null) {
                Toast.makeText(getContext(), "Deleted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
