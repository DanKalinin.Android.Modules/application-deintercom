package application.deintercom;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import application.deintercom.databinding.IntQrCodeFragmentBinding;
import library.deintercom.IntClient;
import library.deintercom.IntDatabase;
import library.deintercom.IntJwt;
import library.deintercom.IntQrCode;
import library.deintercom.IntUsbSerial;
import library.glibext.GeException;

public class IntQrCodeFragment extends IntFragment implements View.OnClickListener, IntContext.Listener {
    public IntQrCodeFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding != null) return binding.getRoot();

        IntContext.shared().listeners.add(this);

        binding = IntQrCodeFragmentBinding.inflate(inflater, container, false);
        binding.toolbar.setNavigationOnClickListener(this);
        binding.error.buttonRetry.setOnClickListener(this);
        get();
        return binding.getRoot();
    }

    @Override
    public void onPop() {
        IntContext.shared().listeners.remove(this);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.error.buttonRetry)) {
            get();
        } else {
            IntNavigation.shared().pop(this);
        }
    }

    @Override
    public void call(IntContext sender, IntUsbSerial serial, byte a) {

    }

    @Override
    public void commit(IntContext sender, IntDatabase database) {

    }

    @Override
    public void call(IntContext sender, IntClient client, byte a) {

    }

    @Override
    public void ip(IntContext sender, IntClient client, String local, String external) {
        get();
    }

    @Override
    public void accept(IntContext sender, IntClient client, String id, byte a) {

    }

    @Override
    public void progress(IntContext sender, IntClient client, long current, long total, int tag) {

    }

    @Override
    public void preferences(IntContext sender, SharedPreferences preferences, String key) {
        if (key.equals(IntContext.KEY_BACKEND)) {
            IntNavigation.shared().pop(this);
        }
    }

    public void loading(boolean loading) {
        binding.progressIndicator.setVisibility(View.GONE);
        binding.linearLayoutQr.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        if (loading) {
            binding.progressIndicator.setVisibility(View.VISIBLE);
        }
    }

    public void success(IntQrCode qrCode) {
        try {
            binding.progressIndicator.setVisibility(View.GONE);
            binding.linearLayoutQr.setVisibility(View.GONE);
            binding.error.linearLayout.setVisibility(View.GONE);

            if ((qrCode.local != null) && (qrCode.external != null)) {
                String token = IntJwt.qrCodeEncode(qrCode, BuildConfig.APPLICATION_ID);

                BarcodeEncoder encoder = new BarcodeEncoder();
                Bitmap bitmap = encoder.encodeBitmap(token, BarcodeFormat.QR_CODE, 800, 800);

                binding.imageViewQr.setImageBitmap(bitmap);
                binding.linearLayoutQr.setVisibility(View.VISIBLE);
            } else {
                binding.error.textView.setText(R.string.enable_upnp_on_your_router);
                binding.error.linearLayout.setVisibility(View.VISIBLE);
            }
        } catch (GeException | WriterException exception) {
            binding.error.textView.setText(R.string.unknown_error);
            binding.error.linearLayout.setVisibility(View.VISIBLE);
        }
    }

    public void exception(String text) {
        binding.progressIndicator.setVisibility(View.GONE);
        binding.linearLayoutQr.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        binding.error.textView.setText(text);
        binding.error.linearLayout.setVisibility(View.VISIBLE);
    }

    public void get() {
        loading(true);
        IntContext.shared().pinger.current.qrCodeAsync((qrCode, exception) -> {
            loading(false);

            if (qrCode == null) {
                exception(getString(R.string.insert_a_usb_controller));
            } else {
                success(qrCode);
            }
        });
    }
}
