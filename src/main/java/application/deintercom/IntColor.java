package application.deintercom;

import library.deintercom.IntCall;
import library.java.lang.LjlObject;

public class IntColor extends LjlObject {
    public static final String RED = "red";
    public static final String GREEN = "green";
    public static final String BLUE = "blue";

    public static int colorToId(String color) {
        if (RED.equals(color)) return R.id.menuItemRed;
        if (GREEN.equals(color)) return R.id.menuItemGreen;
        if (BLUE.equals(color)) return R.id.menuItemBlue;
        return R.id.menuItemPurple;
    }

    public static String idToColor(int id) {
        if (id == R.id.menuItemRed) return RED;
        if (id == R.id.menuItemGreen) return GREEN;
        if (id == R.id.menuItemBlue) return BLUE;
        return null;
    }

    public static int colorToStyle(String color) {
        if (RED.equals(color)) return R.style.Theme_Red;
        if (GREEN.equals(color)) return R.style.Theme_Green;
        if (BLUE.equals(color)) return R.style.Theme_Blue;
        return R.style.Theme_Main;
    }

    public static int callActionToColor(int action) {
        if (action == IntCall.ACTION_ANSWER) return R.color.green;
        if (action == IntCall.ACTION_OPEN) return R.color.blue;
        return R.color.red;
    }

    public static int callActionToColorV1(int action) {
        int id = callActionToColor(action);
        return IntContext.shared().application.getColor(id);
    }
}
