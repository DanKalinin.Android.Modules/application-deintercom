package application.deintercom;

import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import library.deintercom.IntMessage;

public class IntViewHolder extends RecyclerView.ViewHolder {
    public TextView textViewDate;
    public TextView textViewTime;
    public IntMessage message;
    public IntMessage previous;

    public IntViewHolder(View itemView) {
        super(itemView);

        textViewDate = itemView.findViewById(R.id.textViewDate);
        textViewTime = itemView.findViewById(R.id.textViewTime);
    }

    public void setMessage(IntMessage message) {
        this.message = message;

        String textDate = DateUtils.formatDateTime(itemView.getContext(), message.timestamp, DateUtils.FORMAT_SHOW_DATE);
        String textTime = DateUtils.formatDateTime(itemView.getContext(), message.timestamp, DateUtils.FORMAT_SHOW_TIME);

        textViewDate.setText(textDate);
        textViewTime.setText(textTime);
    }

    public void setPrevious(IntMessage previous) {
        this.previous = previous;

        if (previous == null) {
            textViewDate.setVisibility(View.VISIBLE);
        } else {
            long when = System.currentTimeMillis() - (message.timestamp - previous.timestamp);
            int visibility = DateUtils.isToday(when) ? View.GONE : View.VISIBLE;
            textViewDate.setVisibility(visibility);
        }
    }
}
