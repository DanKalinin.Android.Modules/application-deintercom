package application.deintercom;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import library.androidx.fragment.app.LafaDialogFragment;

public class IntErrorFragment extends LafaDialogFragment implements DialogInterface.OnClickListener {
    public String text;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new MaterialAlertDialogBuilder(requireContext())
                .setMessage(text)
                .setPositiveButton(R.string.ok, this)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    public static IntErrorFragment newInstance(String text) {
        IntErrorFragment ret = new IntErrorFragment();
        ret.text = text;
        return ret;
    }
}
