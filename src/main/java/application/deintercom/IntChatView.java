package application.deintercom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import java.util.ArrayList;
import application.deintercom.databinding.IntChatViewBinding;
import library.com.google.android.material.bottomnavigation.LcgambBottomNavigationView;

public class IntChatView extends LcgambBottomNavigationView implements View.OnClickListener, IntMeter.Listener {
    public interface Listener {
        void onRecord(IntChatView sender);
        void onSend(IntChatView sender);
        void onCancel(IntChatView sender);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void onRecord(IntChatView sender) {
            for (Listener listener : this) {
                listener.onRecord(sender);
            }
        }

        @Override
        public void onSend(IntChatView sender) {
            for (Listener listener : this) {
                listener.onSend(sender);
            }
        }

        @Override
        public void onCancel(IntChatView sender) {
            for (Listener listener : this) {
                listener.onCancel(sender);
            }
        }
    }

    public IntChatViewBinding binding;
    public Listeners listeners;

    public IntChatView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = LayoutInflater.from(context);
        binding = IntChatViewBinding.inflate(inflater, this);
        binding.buttonRecord.setOnClickListener(this);
        binding.buttonSend.setOnClickListener(this);
        binding.buttonCancel.setOnClickListener(this);

        listeners = new Listeners();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.buttonRecord)) {
            listeners.onRecord(this);
        } else if (v.equals(binding.buttonSend)) {
            stop();
            listeners.onSend(this);
        } else if (v.equals(binding.buttonCancel)) {
            stop();
            listeners.onCancel(this);
        }
    }

    @Override
    public void onAmplitude(IntMeter sender, float amplitude) {
        binding.waveView.add(amplitude);
    }

    public void record() {
        binding.buttonRecord.setVisibility(GONE);
        binding.buttonSend.setVisibility(VISIBLE);
        binding.buttonCancel.setVisibility(VISIBLE);
        binding.waveView.setVisibility(VISIBLE);

        IntMeter.shared().listeners.remove(this);
        IntMeter.shared().listeners.add(this);
        IntMeter.shared().startRecording();
    }

    public void stop() {
        binding.buttonRecord.setVisibility(VISIBLE);
        binding.buttonSend.setVisibility(GONE);
        binding.buttonCancel.setVisibility(GONE);
        binding.waveView.setVisibility(GONE);
        binding.waveView.clear();

        IntMeter.shared().stop();
    }
}
