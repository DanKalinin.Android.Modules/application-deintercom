package application.deintercom;

import android.Manifest;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;
import application.deintercom.databinding.IntMainActivityBinding;
import library.deintercom.IntBackend;
import library.deintercom.IntClient;
import library.deintercom.IntDatabase;
import library.deintercom.IntPinger;
import library.deintercom.IntUsbSerial;
import library.glibext.GeException;

public class IntMainActivity extends IntActivity implements NavigationBarView.OnItemSelectedListener, NavigationView.OnNavigationItemSelectedListener, ActivityResultCallback<Boolean>, IntContext.Listener, IntDownloader.Listener {
    public IntMainActivityBinding binding;
    public ActivityResultLauncher<String> launcher;
    public IntBackend[] backends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        client();

        IntContext.shared().listeners.add(this);
        IntDownloader.shared().listeners.add(this);

        IntNavigation.shared().fragmentManager = getSupportFragmentManager();

        binding = IntMainActivityBinding.inflate(getLayoutInflater());
        binding.bottomNavigationView.setOnItemSelectedListener(this);
        binding.bottomNavigationView.setSelectedItemId(IntMenu.main().item);
        binding.navigationView.setNavigationItemSelectedListener(this);
        setContentView(binding.getRoot());

        launcher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), this);

        backendSelect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        IntContext.shared().listeners.remove(this);
        IntDownloader.shared().listeners.remove(this);

        IntNavigation.shared().close();
    }

    @Override
    public void onBackPressed() {
        if (IntNavigation.shared().back()) return;
        super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuItemIntercom) {
            IntNavigation.shared().set(IntNavigation.shared().stackChat);
            IntMenu.main().item = R.id.menuItemIntercom;
        } else if (item.getItemId() == R.id.menuItemSettings) {
            IntNavigation.shared().set(IntNavigation.shared().stackSettings);
            IntMenu.main().item = R.id.menuItemSettings;
        } else if (item.getItemId() == R.id.menuItemAdd) {
            launcher.launch(Manifest.permission.CAMERA);
            binding.drawerLayout.closeDrawer(binding.navigationView);
        } else {
            IntBackend backend = backends[item.getOrder()];

            IntPlayer.shared().reset();
            IntDownloader.shared().abort();
            if (IntContext.shared().pinger != null) IntContext.shared().pinger.close();
            String baseLocal = "https://" + backend.local + ":60000";
            IntClient local = IntClient.client(baseLocal, IntCredentials.shared().id, backend.id, IntContext.shared().databaseClient);
            local.callsV1Async(null);
            local.filesV1Async("music", null);
            local.passwordsAsync(null);
            local.frontendsAsync(null);
            String baseExternal = backend.external == null ? null : "https://" + backend.external + ":60000";
            IntClient external = backend.external == null ? null : IntClient.client(baseExternal, IntCredentials.shared().id, backend.id, IntContext.shared().databaseClient);
            if (backend.external != null) external.callsV1Async(null);
            if (backend.external != null) external.filesV1Async("music", null);
            if (backend.external != null) external.passwordsAsync(null);
            if (backend.external != null) external.frontendsAsync(null);
            IntContext.shared().pinger = IntPinger.pinger(local, external, local);

            SharedPreferences.Editor editor = IntContext.shared().preferences.edit();
            editor.putString(IntContext.KEY_BACKEND, backend.id);
            editor.apply();

            binding.drawerLayout.closeDrawer(binding.navigationView);
        }

        return true;
    }

    @Override
    public void onActivityResult(Boolean result) {
        if (result) {
            IntAddFragment fragment = new IntAddFragment();
            IntNavigation.shared().push(fragment, IntNavigation.shared().stackCurrent);
        } else if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            IntRationaleFragment fragment = IntRationaleFragment.newInstance(launcher, Manifest.permission.CAMERA);
            fragment.show(getSupportFragmentManager(), null);
        } else {
            IntDeniedFragment fragment = IntDeniedFragment.newInstance(Manifest.permission.CAMERA);
            fragment.show(getSupportFragmentManager(), null);
        }
    }

    @Override
    public void call(IntContext sender, IntUsbSerial serial, byte a) {

    }

    @Override
    public void commit(IntContext sender, IntDatabase database) {
        backendSelect();
    }

    @Override
    public void call(IntContext sender, IntClient client, byte a) {

    }

    @Override
    public void ip(IntContext sender, IntClient client, String local, String external) {

    }

    @Override
    public void accept(IntContext sender, IntClient client, String id, byte a) {

    }

    @Override
    public void progress(IntContext sender, IntClient client, long current, long total, int tag) {

    }

    @Override
    public void preferences(IntContext sender, SharedPreferences preferences, String key) {
        if (key.equals(IntContext.KEY_BACKEND)) {
            for (int i = 0; i < backends.length; i++) {
                IntBackend backend = backends[i];
                boolean checked = (sender.pinger != null) && backend.id.equals(sender.pinger.current.password);
                MenuItem item = binding.navigationView.getMenu().getItem(i);
                item.setChecked(checked);
            }
        }
    }

    @Override
    public void file(IntDownloader sender) {

    }

    @Override
    public void exception(IntDownloader sender, GeException exception) {
        IntErrorFragment fragment = IntErrorFragment.newInstance(exception.getLocalizedMessage());
        fragment.show(getSupportFragmentManager(), null);
    }

    public void backendSelect() {
        try {
            backends = IntContext.shared().databaseClient.backendSelect(null);
            binding.navigationView.getMenu().clear();

            if (backends.length > 0) {
                for (int i = 0; i < backends.length; i++) {
                    IntBackend backend = backends[i];
                    String title = (backend.name == null) ? getString(R.string.intercom) : backend.name;
                    boolean checked = (IntContext.shared().pinger != null) && backend.id.equals(IntContext.shared().pinger.current.password);
                    MenuItem item = binding.navigationView.getMenu().add(Menu.NONE, Menu.NONE, i, title);
                    item.setChecked(checked);
                }

                binding.navigationView.getMenu().setGroupCheckable(Menu.NONE, true, true);
            } else {
                binding.navigationView.inflateMenu(R.menu.drawer);
            }
        } catch (GeException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void client() {
        try {
            if (IntContext.shared().pinger != null) return;
            String backendId = IntContext.shared().preferences.getString(IntContext.KEY_BACKEND, null);
            if (backendId == null) return;
            IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(backendId);
            if (backends.length == 0) return;
            IntBackend backend = backends[0];
            String baseLocal = "https://" + backend.local + ":60000";
            IntClient local = IntClient.client(baseLocal, IntCredentials.shared().id, backend.id, IntContext.shared().databaseClient);
            local.callsV1Async(null);
            local.filesV1Async("music", null);
            local.passwordsAsync(null);
            local.frontendsAsync(null);
            String baseExternal = backend.external == null ? null : "https://" + backend.external + ":60000";
            IntClient external = backend.external == null ? null : IntClient.client(baseExternal, IntCredentials.shared().id, backend.id, IntContext.shared().databaseClient);
            if (backend.external != null) external.callsV1Async(null);
            if (backend.external != null) external.filesV1Async("music", null);
            if (backend.external != null) external.passwordsAsync(null);
            if (backend.external != null) external.frontendsAsync(null);
            IntContext.shared().pinger = IntPinger.pinger(local, external, local);
        } catch (GeException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
