package application.deintercom;

import android.view.View;
import android.widget.TextView;
import com.google.android.material.card.MaterialCardView;
import library.deintercom.IntCall;
import library.deintercom.IntMessage;

public class IntViewHolderCall extends IntViewHolder {
    public TextView textViewBody;
    public MaterialCardView cardViewAction;

    public IntViewHolderCall(View itemView) {
        super(itemView);

        textViewBody = itemView.findViewById(R.id.textViewBody);
        cardViewAction = itemView.findViewById(R.id.cardViewAction);
    }

    @Override
    public void setMessage(IntMessage message) {
        super.setMessage(message);

        String textBody = (message.call.action == IntCall.ACTION_NONE) ? itemView.getContext().getString(R.string.missed_call) : itemView.getContext().getString(R.string.accepted_call);
        int colorAction = IntColor.callActionToColorV1(message.call.action);

        textViewBody.setText(textBody);
        cardViewAction.setCardBackgroundColor(colorAction);
    }
}
