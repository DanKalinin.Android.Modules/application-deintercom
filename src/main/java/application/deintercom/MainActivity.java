package application.deintercom;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.ComponentName;
import android.content.res.Resources;
import android.media.AudioAttributes;
import android.media.AudioDeviceInfo;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRouting;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

import application.deintercom.databinding.ActivityMainBinding;
import library.apnext.ApnCredential;
import library.asteriskext.AstrDatabase;
import library.asteriskext.AstrSipPeer;
import library.deintercom.IntApnClient;
import library.deintercom.IntClient;
import library.deintercom.IntFrbClient;
import library.deintercom.IntIgd;
import library.deintercom.IntPinger;
import library.deintercom.IntProgrammer;
import library.deintercom.IntPtyFs;
import library.deintercom.IntPushAccept;
import library.deintercom.IntPushCall;
import library.deintercom.IntPushIP;
import library.deintercom.IntServer;
//import library.deintercom.IntSipCall;
import library.deintercom.IntSipAgent;
import library.deintercom.IntTest;
import library.ffmpegext.MpgBin;
import library.firebaseext.FrbCredential;
import library.glibext.GeException;
import library.ptyext.PtySession;

public class MainActivity extends IntActivity implements IntIgd.Listener, Chronometer.OnChronometerTickListener, IntPinger.Listener, ActivityResultCallback<Boolean>, View.OnClickListener {
    ActivityMainBinding binding;

    IntServer server;
    IntFrbClient frbClient;
    IntApnClient apnClient;


    PowerManager.WakeLock wakeLock;
    WifiManager.WifiLock wifiLock;
    WifiManager.MulticastLock lock;
    IntIgd igd;
    MediaPlayer player;
    IntPinger pinger;

    MediaPlayer player1;
    MediaPlayer player2;

    MediaRecorder recorder1;
    MediaRecorder recorder2;

    AudioTrack track1;
    AudioTrack track2;

    public ActivityResultLauncher<String> launcher;

    int account = -1;
    int call = -1;

//    @Override
//    public Resources.Theme getTheme() {
//        Resources.Theme ret = super.getTheme();
//        ret.applyStyle(R.style.Theme_Red, true);
//        return ret;
//    }

    @Override
    public void ip(IntIgd sender, String local, String external) {
        Toast.makeText(this, external, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void error(IntIgd sender, GeException exception) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void pong(IntPinger sender, IntClient current) {
        if (current.equals(sender.local)) {
            Toast.makeText(this, "Local", Toast.LENGTH_SHORT).show();
        } else if (current.equals(sender.external)) {
            Toast.makeText(this, "External", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(Boolean result) {
        System.out.println(1);

//        if (result) {
//            IntAddFragment addFragment = new IntAddFragment();
//            IntNavigation.shared().push(addFragment, IntNavigation.shared().stackCurrent);
//        } else if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
//            IntRationaleDialogFragment dialogFragment = IntRationaleDialogFragment.newInstance(launcher, Manifest.permission.CAMERA);
//            dialogFragment.show(getSupportFragmentManager(), null);
//        } else {
//            IntDeniedDialogFragment dialogFragment = IntDeniedDialogFragment.newInstance(Manifest.permission.CAMERA);
//            dialogFragment.show(getSupportFragmentManager(), null);
//        }

        if (result) {
            // Resume
            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
        } else if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
            IntRationaleFragment fragment = IntRationaleFragment.newInstance(launcher, Manifest.permission.RECORD_AUDIO);
            fragment.show(getSupportFragmentManager(), null);
        } else {
            IntDeniedFragment fragment = IntDeniedFragment.newInstance(Manifest.permission.RECORD_AUDIO);
            fragment.show(getSupportFragmentManager(), null);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.buttonCall)) {
            try {
                account = IntContext.shared().agent.accountAdd("127.0.0.1", IntCredentials.shared().id, IntCredentials.shared().id, false);
                call = IntContext.shared().agent.callMake(account, "127.0.0.1", "6000");
            } catch (GeException exception) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (v.equals(binding.buttonHangup)) {
            try {
                if (call != -1) IntContext.shared().agent.callHangup(call);
                call = -1;
                if (account != -1) IntContext.shared().agent.accountDelete(account);
                account = -1;
            } catch (GeException exception) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (v.equals(binding.buttonColor)) {
            IntColorFragment fragment = new IntColorFragment();
            fragment.show(getSupportFragmentManager(), null);
        } else if (v.equals(binding.buttonUpdate)) {
            IntAvrDude.shared().updateAsync(1, null);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        binding.buttonCall.setOnClickListener(this);
        binding.buttonHangup.setOnClickListener(this);
        binding.buttonColor.setOnClickListener(this);
        binding.buttonUpdate.setOnClickListener(this);
        setContentView(binding.getRoot());

//        ComponentName admin = new ComponentName(this, IntAdminReceiver.class);
//        System.out.println(admin);

//        launcher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), this);
//        launcher.launch(Manifest.permission.RECORD_AUDIO);

//        String url = "https://127.0.0.1:60000";
//        int endIndex = url.length() - 6;
//        String host = url.substring(8, endIndex);
//        System.out.println(host);

//        AudioManager manager = getSystemService(AudioManager.class);
//        AudioDeviceInfo[] infos = manager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
//        IntTest.test();

        IntTest.test();

//        try {
//            String[] argv = {"ffmpeg", "-y", "-f", "s16le", "-ar", "16k", "-ac", "1", "-ss", "2.19", "-to", "5.82", "-i", "/data/data/a.intercom/files/1.pcm", "/data/data/a.intercom/files/1.ogg"};
//            MpgBin.ffmpeg(argv);
//            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
//        } catch (GeException exception) {
//            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//        }

//        String[] argv = {"ffmpeg", "-y", "-f", "s16le", "-ar", "16k", "-ac", "1", "-ss", "2.19", "-to", "5.82", "-i", "/data/data/a.intercom/files/1.pcm", "/data/data/a.intercom/files/1.ogg"};
//        MpgBin.ffmpegAsync(argv, exception -> {
//            if (exception == null) {
//                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

//        mount();

//        play();

//        fork();

//        track();

//        reflect();

//        IntDeniedDialogFragment dialogFragment = IntDeniedDialogFragment.newInstance(Manifest.permission.RECORD_AUDIO);
//        dialogFragment.show(getSupportFragmentManager(), null);

//        int id = getResources().getIdentifier("android.permission.CAMERA", "string", getPackageName());
//        String text = getString(id);
//        System.out.println(1);

//        String tag = BuildConfig.APPLICATION_ID + ":" + PowerManager.WakeLock.class.getSimpleName();
//        PowerManager powerManager = getSystemService(PowerManager.class);
//        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, tag);
//        wakeLock.acquire();
//
//        WifiManager wifiManager = getSystemService(WifiManager.class);
//        wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, BuildConfig.APPLICATION_ID);
//        wifiLock.acquire();
//
//        System.out.println(1);

//        IntClient local = IntClient.client("https://192.168.0.2:60000", "909e6353-07d1-390f-a956-5773dbbc2125", "1", null);
//        IntClient external = IntClient.client("https://92.101.233.149:60000", "909e6353-07d1-390f-a956-5773dbbc2125", "1", null);
////        pinger = IntPinger.pinger(local, external, null);
////        pinger.listeners.add(this);
//
//        IntClients clients = new IntClients();
//        clients.add(local);
//        clients.add(external);
//
//        clients.answerAsync((byte) 1, (client, exception) -> {
//            System.out.println(1);
//        });

//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        Ringtone ringtone = RingtoneManager.getRingtone(this, uri);
//        ringtone.play();

//        try {
//            File file = new File(getFilesDir(), "1/Numb.mp3");
//
//            player = new MediaPlayer();
//            player.setDataSource(file.toString());
//            player.prepare();
//            player.start();
//
//            long base = SystemClock.elapsedRealtime() + player.getDuration() - player.getCurrentPosition();
//
//            binding.chronometer.setBase(base);
//            binding.chronometer.setOnChronometerTickListener(this);
//            binding.chronometer.stop();
//            binding.chronometer.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        String text = SqleFormat.text("a");
//        String texts = SqleFormat.texts(new String[]{"a", "b", "c"});
//        String ints = SqleFormat.ints(new int[]{1, 2, 3});
//        System.out.println(1);

//        try {
//            File file = new File(getFilesDir(), "intsqledatabase.client.sqlite3");
//            IntDatabase database = IntDatabase.database(file.toString());
//
////            int n = database.selectInt("count(*) FROM backend")[0];
////            System.out.println(1);
//
//            String[] ids = database.selectText("id FROM backend");
//            IntBackend[] backends = database.backendSelect("WHERE id IN " + SqleFormat.texts(ids));
//            System.out.println(1);
//        } catch (GeException exception) {
//            exception.printStackTrace();
//        }

//        try {
//            File file = new File(getFilesDir(), "intsqledatabase.client.sqlite3");
//            IntDatabase database = IntDatabase.database(file.toString());
//
//            IntBackend backend1 = new IntBackend();
//            backend1.id = "1";
//            backend1.name = "a";
//            backend1.local = "192.168.0.1";
//            database.backendUpsert(backend1);
//
//            IntBackend backend2 = new IntBackend();
//            backend2.id = "2";
//            backend2.local = "192.168.0.2";
//            database.backendUpsert(backend2);
//
////            SQLiteDatabase db = SQLiteDatabase.openDatabase(file.toString(), null, SQLiteDatabase.OPEN_READWRITE);
////            db.execSQL("DELETE FROM backend");
//        } catch (GeException exception) {
//            exception.printStackTrace();
//        }

//        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
//            if (task.isSuccessful()) {
//                Toast.makeText(this, task.getResult(), Toast.LENGTH_SHORT).show();
//                Log.i("INTERCOM", task.getResult());
//            } else {
//                Toast.makeText(this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

//        Toast.makeText(this, FirebaseApp.getInstance().getOptions().getProjectId(), Toast.LENGTH_SHORT).show();

//        mount();

//        try {
//            File file = new File(getFilesDir(), "usr/var/lib/asterisk/astrsqledatabase.sqlite3");
//            AstrDatabase database = AstrDatabase.database(file.toString());
//
//            AstrSipPeer sipPeer = new AstrSipPeer();
//            sipPeer.name = "phone-x";
//            sipPeer.host = "dynamic";
//            sipPeer.context = "local";
//            sipPeer.secret = "1";
//            sipPeer.type = "friend";
//
//            database.sipPeerUpsert(sipPeer);
//
//            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
//        } catch (GeException exception) {
//            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//        }

//        IntQrCode qrCode = new IntQrCode();
//        qrCode.local = "192.168.0.1";
//        qrCode.external = "192.168.0.5";
//        qrCode.username = "F2C24625-8BF5-4FD5-B387-05F9CBFA8719";
//        qrCode.password = "12345678";
//
//        try {
//            String token = IntJwt.qrCodeEncode(qrCode, BuildConfig.APPLICATION_ID);
//            Toast.makeText(this, token, Toast.LENGTH_SHORT).show();
//        } catch (GeException exception) {
//            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//        }

        // UsbManager manager = context.getSystemService(UsbManager.class);
        //

//        WifiManager manager = getSystemService(WifiManager.class);
//
//        lock = manager.createMulticastLock(BuildConfig.APPLICATION_ID);
//        lock.acquire();
//
//        igd = IntIgd.igd("wlan0", 8080, 5060);
//        igd.listeners.add(this);

//        final WifiManager manager = (WifiManager) super.getSystemService(WIFI_SERVICE);
//        final DhcpInfo dhcp = manager.getDhcpInfo();
//        final String ip = Formatter.formatIpAddress(dhcp.ipAddress);
//        final String address = Formatter.formatIpAddress(dhcp.gateway);

//        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        // fea9edcc058911d2 - 64 bits

//        String mostSigBits = androidId.substring(0, 8);
//        String leastSigBits = androidId.substring(8, 16);
//        long mostSigBits1 = Long.parseLong(mostSigBits, 16);
//        long leastSigBits1 = Long.parseLong(leastSigBits, 16);
//        UUID uuid = new UUID(mostSigBits1, leastSigBits1);

        // 00000000-fea9-edcc-0000-0000058911d2 - 128 bits
        // 00000000-fea9-edcc-0000-0000058911d2

        // 00000000-0589-11d2-0000-0000fea9edcc
        // 00000000-0589-11d2-0000-0000fea9edcc

//        UUID backendId = UUID.nameUUIDFromBytes(androidId.getBytes());

        // ad3eb981-bc25-33ea-8b87-b9aa4d039b65
        // ad3eb981-bc25-33ea-8b87-b9aa4d039b65
        // ad3eb981-bc25-33ea-8b87-b9aa4d039b65

//        System.out.println("OK");

        // *****

//        FrbCredential credential = new FrbCredential();
//        credential.privateKeyId = "a6677f2410f9a4ae2a969e8a9e0882cb346fd010";
//        credential.privateKey = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDYe4C7pWingn4a\nIUpsvZwBbJEE6iSp1FEllGEVvlUsGmBeGhsgSijI0IaYU02Rz5YdIAUC69IY0OwO\nm0/GKjldF3cPaEsn0aUhqk7K+TDBECfKaYUJLzNFwTCBAQeni+T7VWaQL2pWJsJC\nk8goWXUVSgATtTCV6nLXDs21Q0C2nCu2ILSmD9P5uSCMqFQ7rKGj0lIlji/Fnmv8\nRTCCtzNErKtH13h+TFhvXw2+TDcOnKT1tewyZ1Zg3wtLFqsBNG68JmfzD3j/K0Ur\n7vyGUnowwIA0RPbj+aTmBOEfcOz9nQEFL0SjiHxPcO4fIR2Sk8VprErQqY4GslWl\ncj69pQwfAgMBAAECggEABHN5oKIUhkkxFZF0qcq2P/M/AhZcXZ+PcZHc8ykAMFF0\nhMrNnvrXXtWmxtADqslVYVDsuXQm1DrLd7tqxvzMRCU83fS7gtyVJB2P1C2OepG9\n97P2t8sgJ0GDIUvnn+LI+P/3BWTsL0xJtzu5pFa+iQsF+GHD13lEFjboNN9mn7qJ\nnzZwrDmBVUhtCKrqpRV4V3y9R4ESgzfG7MR2yhS+iRRH+6JhbsjBhOSYsKeoC5Sf\n+IeoQUtGHb8glIFTm5eZyqACON9mv0b3dvfFnKcPIw5AryoGiUi8PKVuE11j3KHX\njfQ6BUkube01ld9wkOdNOfbqexQFCMmWIsfyDyp0qQKBgQDs+IegYYFlVg29tShi\n/SjELKKjo9CZIW70PB5iaJFklK1IC29H97Ph907nhmz/JoPiXZ/hTOyv/B/97+4O\nzcuGOSxJufgDld1wof5x8HQXwlXYxC3k0kuP4LO28IGrR0FpbE0zu10wVn2CTQua\n2b2lUG05O1/lFZc3Fo87dgdsZwKBgQDp3cm8dw7A8Qmm1l6KjyPwSCaDDL19gjf9\nZOL4je9bBbbNi/CtsTXmVvv+2CaEtYeCcikU4TLy88GOCxlj4wIOLhfhEk/cQJt8\nlwDdVjFBo0CfYSMAV4qlhjAQ3Bv0Smja+//+MP0XIxmzPPGdollIa0qSrTmuG6OK\nZdXW5d4PiQKBgAl1gjRsr/WTkcjYylvpLw9lmLhfeZwjMYKeQ7g55FPXe3M8wZxl\nztBeRtAHFaK5rtoCUrdzkp8TuVMxmJcOr+kTryjiO3wpJ0WvsbBqoLeyEiss6YgQ\nvONpXMMYppRnq7zBjfNar0WWrjvdR3FO77sBktqN/C3wxrB0PhNpirJRAoGBAIHC\nkKukd6gwVYWfjgddUriAn/W4vO8FsKFFN/ZncnZTnlFAI0OMfvaWLu4A6qZZvp1v\nUEIIjedlvnBc0jKtAzfw9UjpiywPIPapJk6pA12DAlksYWpQKFqdMNA6ALy0CKfw\niOJ2Ngd/b0QXPYyURuu6tD7cJJPpO/2PcW3yTxrZAoGBAJzjviJz3SOAeaZM4gWt\nQCBkg5Q4Y/ACF7P/ZzC3oo8BtQ8it2IX4CpTz8YnT2RC6QVHurl5U3gG2M+2R9uQ\nTJ2I8EgyDVhr/3YqQCCn9zLtmA0KptXs2fvqRnlH/JwW6RVSLTMj2Zz8v3u/erAm\npXC6OofpWNSuR/3tbPSk/KOA\n-----END PRIVATE KEY-----";
//        credential.clientEmail = "firebase-adminsdk-monla@intercom-e57c6.iam.gserviceaccount.com";
//        frbClient = IntFrbClient.client("https://fcm.googleapis.com", credential);
//
//        IntVoIPPush voipPush = new IntVoIPPush();
//        voipPush.project = "intercom-e57c6";
//        voipPush.token = "dN25DlC0TQOFQAQnqc29Wo:APA91bHiBMRMN5S1sXJjFVkw6U4cabS0e-eKX82RwaebgXDPdYXRFhpCn9phetSW_pchYtjTlSE-vquKmDuKTd5CcjrtzAaiP-8lY9jTSieZUaHRkWB2qNI9RIBQtg34tvY7Je2VBI4E";
//        voipPush.id = "5";
//        voipPush.a = 1;
//        frbClient.sendVoIP(voipPush);
//
//        IntPushCall pushCall = new IntPushCall();
//        pushCall.project = "intercom-e57c6";
//        pushCall.token = "dN25DlC0TQOFQAQnqc29Wo:APA91bHiBMRMN5S1sXJjFVkw6U4cabS0e-eKX82RwaebgXDPdYXRFhpCn9phetSW_pchYtjTlSE-vquKmDuKTd5CcjrtzAaiP-8lY9jTSieZUaHRkWB2qNI9RIBQtg34tvY7Je2VBI4E";
//        pushCall.id = "id";
//        pushCall.a = 1;
//        pushCall.local = "192.168.0.1";
//        pushCall.external = "192.168.0.2";
//        frbClient.pushCall(pushCall);
//
//        IntPushIP pushIP = new IntPushIP();
//        pushIP.project = "intercom-e57c6";
//        pushIP.token = "dN25DlC0TQOFQAQnqc29Wo:APA91bHiBMRMN5S1sXJjFVkw6U4cabS0e-eKX82RwaebgXDPdYXRFhpCn9phetSW_pchYtjTlSE-vquKmDuKTd5CcjrtzAaiP-8lY9jTSieZUaHRkWB2qNI9RIBQtg34tvY7Je2VBI4E";
//        pushIP.id = "id";
//        pushIP.local = "192.168.0.1";
//        pushIP.external = "192.168.0.2";
//        frbClient.pushIP(pushIP);
//
//        IntPushAccept pushAccept = new IntPushAccept();
//        pushAccept.project = "intercom-e57c6";
//        pushAccept.token = "dN25DlC0TQOFQAQnqc29Wo:APA91bHiBMRMN5S1sXJjFVkw6U4cabS0e-eKX82RwaebgXDPdYXRFhpCn9phetSW_pchYtjTlSE-vquKmDuKTd5CcjrtzAaiP-8lY9jTSieZUaHRkWB2qNI9RIBQtg34tvY7Je2VBI4E";
//        pushAccept.backendId = "backendId";
//        pushAccept.frontendId = "frontendId";
//        pushAccept.a = 2;
//        frbClient.pushAccept(pushAccept);
//
//        Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();

        // *****

//        try {
//            ApnCredential credential = new ApnCredential();
//            credential.key = "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgYfABlKXOZFf0E8PN\npjQWvQiq9Ft4aJAC5P6izSosApKgCgYIKoZIzj0DAQehRANCAAQVqG1Ma+IHb3vB\nwtXYcXbPVnO6whi/TPlMX3vWDQNLQwouI5HCFnyay6yOynDImEWjPByofBNmQanA\na/1IJTev\n-----END PRIVATE KEY-----";
//            credential.keyId = "TKD5MLALF5";
//            credential.teamId = "4W2S6G9WUZ";
//            apnClient = IntApnClient.client("https://api.development.push.apple.com", credential);
//
//            IntVoIPPush voipPush = new IntVoIPPush();
//            voipPush.project = "com.de.DEIntercomDemo";
//            voipPush.token = "2fd0a9a5ad600194fdefb7c540b0768fca5bfc27d35fddedf43f8b6a9618ef7d";
//            voipPush.id = "DD3C97E4-057E-4325-8360-2232A030C7E3";
//            voipPush.a = 1;
//            apnClient.sendVoIP(voipPush);
//
//            IntPushCall pushCall = new IntPushCall();
//            pushCall.project = "com.de.DEIntercomDemo";
//            pushCall.token = "2fd0a9a5ad600194fdefb7c540b0768fca5bfc27d35fddedf43f8b6a9618ef7d";
//            pushCall.id = "id";
//            pushCall.a = 1;
//            pushCall.local = "192.168.0.1";
//            pushCall.external = "192.168.0.2";
//            apnClient.pushCall(pushCall);
//
//            IntPushIP pushIP = new IntPushIP();
//            pushIP.project = "com.de.DEIntercomDemo";
//            pushIP.token = "2fd0a9a5ad600194fdefb7c540b0768fca5bfc27d35fddedf43f8b6a9618ef7d";
//            pushIP.id = "id";
//            pushIP.local = "192.168.0.1";
//            pushIP.external = "192.168.0.2";
//            apnClient.pushIP(pushIP);
//
//            IntPushAccept pushAccept = new IntPushAccept();
//            pushAccept.project = "com.de.DEIntercomDemo";
//            pushAccept.token = "2fd0a9a5ad600194fdefb7c540b0768fca5bfc27d35fddedf43f8b6a9618ef7d";
//            pushAccept.backendId = "backendId";
//            pushAccept.frontendId = "frontendId";
//            pushAccept.a = 1;
//            apnClient.pushAccept(pushAccept);
//
//            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
//        } catch (GeException exception) {
//            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//        }

        // *****

//        try {
//            File file = new File(getFilesDir(), "intsqledatabase.sqlite3");
//            IntDatabase database = IntDatabase.database(file.toString());
//            server = IntServer.server(60000, database, null);
//            IntClient client = IntClient.client("http://127.0.0.1:60000", database);
//
////            client.answerAsync((byte) 1, exception -> {
////                if (exception == null) {
////                    Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
////                } else {
////                    Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
////                }
////            });
//
//            client.readAsync(exception -> {
//                if (exception == null) {
//                    Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                }
//            });
//        } catch (GeException exception) {
//            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//        }

//        Scanner s = new Scanner("");
//        s.close();

//        IntContext.shared().startService(this);

//        IntentFilter filter = new IntentFilter();
//        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
//        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
//        registerReceiver(receiver, filter);

//        UsbManager manager = (UsbManager) getSystemService(USB_SERVICE);
//        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
//        for (UsbDevice device : deviceList.values()) {
//            String text = "";
//            text += "vendor-id" + " - " + device.getVendorId() + "\n";
//            text += "product-id" + " - " + device.getProductId() + "\n";
//            text += "class" + " - " + device.getDeviceClass() + "\n";
//            text += "subclass" + " - " + device.getDeviceSubclass() + "\n";
//            text += "protocol" + " - " + device.getDeviceProtocol() + "\n";
//            text += "\n";
//            text += "device-name" + " - " + device.getDeviceName() + "\n";
//            text += "product-name" + " - " + device.getProductName() + "\n";
//            text += "manufacturer-name" + " - " + device.getManufacturerName() + "\n";
//            text += "serial-number" + " - " + device.getSerialNumber() + "\n";
//            text += "\n";
//            text += "string" + " - " + device.toString() + "\n";
////            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
//        }
//
//        // /dev/bus/usb/001/002
//        // vendor-id :product-id [:class :subclass :protocol]
//
//        try {
//            IntSerial serial = IntSerial.serial("/dev/ttyUSB0");
//            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
//        } catch (GeException exception) {
//            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//        }

//        File f = new File("assets://1.txt");
//        boolean exists = f.exists();
//        System.out.println(exists);

//        mount();
    }

    @Override
    public void onChronometerTick(Chronometer chronometer) {
        System.out.println(1);
        Toast.makeText(this, "" + player.getCurrentPosition(), Toast.LENGTH_SHORT).show();
    }

    public void mount() {
        IntPtyFs.mountAsync(getFilesDir().getAbsolutePath(), exception -> {
            if (exception == null) {
                peerUpsert();
            } else {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void peerUpsert() {
        try {
            File astrFile = new File(getFilesDir(), "usr/var/lib/asterisk/astrsqledatabase.sqlite3");
            AstrDatabase astrDatabase = AstrDatabase.database(astrFile.toString());
            AstrSipPeer peer = new AstrSipPeer();
            peer.name = "backend";
            peer.host = "dynamic";
            peer.context = "local";
            peer.secret = IntCredentials.shared().id;
            peer.type = "friend";
            astrDatabase.sipPeerUpsert(peer);

            AstrSipPeer peer1 = new AstrSipPeer();
            peer1.name = IntCredentials.shared().id;
            peer1.host = "dynamic";
            peer1.context = "local";
            peer1.secret = IntCredentials.shared().id;
            peer1.type = "friend";
            astrDatabase.sipPeerUpsert(peer1);

            session();
        } catch (GeException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void session() {
        try {
            PtySession session = PtySession.session(BuildConfig.APPLICATION_ID);
            call();
        } catch (GeException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void call() {
        try {
            IntContext.shared().agent.accountAdd("127.0.0.1", "backend", IntCredentials.shared().id, false);
        } catch (GeException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void play() {
        try {
            AudioManager manager = getSystemService(AudioManager.class);
            AudioDeviceInfo[] infos = manager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);

            for (AudioDeviceInfo info : infos) {
                int type = info.getType();
                if (info.getType() == AudioDeviceInfo.TYPE_BUILTIN_SPEAKER) {
                    if (player1 != null) return;
                    player1 = new MediaPlayer();
                    File path = new File(getFilesDir(), "1.mp3");
                    player1.setDataSource(path.toString());
                    if (!player1.setPreferredDevice(info)) return;
                    player1.prepare();
                    player1.start();
                } else if (info.getType() == AudioDeviceInfo.TYPE_USB_HEADSET) {
                    if (player2 != null) return;
                    player2 = new MediaPlayer();
                    File path = new File(getFilesDir(), "2.mp3");
                    player2.setDataSource(path.toString());
                    if (!player2.setPreferredDevice(info)) return;
                    player2.prepare();
                    player2.start();
                }
            }
        } catch (IOException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void record() {
        AudioManager manager = getSystemService(AudioManager.class);
        AudioDeviceInfo[] infos = manager.getDevices(AudioManager.GET_DEVICES_INPUTS);

        for (AudioDeviceInfo info : infos) {
            if (info.getType() == AudioDeviceInfo.TYPE_BUILTIN_MIC) {
                if (recorder1 != null) return;
                recorder1 = new MediaRecorder();
            }
        }
    }

    public void fork() {
        if (IntTest.fork() > 0) {
            // Parent
            Log.i("Intercom", "Parent");
        } else {
            // Child
            Log.i("Intercom", "Child");
            play();
        }
    }

    public void track() {
        AudioManager manager = getSystemService(AudioManager.class);
        AudioDeviceInfo[] infos = manager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);

        for (AudioDeviceInfo info : infos) {
            if (info.getType() == AudioDeviceInfo.TYPE_BUILTIN_SPEAKER) {
                new Thread(() -> {
                    try {
                        File file = new File(getFilesDir(), "1.wav");
                        byte[] bytes = new byte[(int) file.length()];

                        FileInputStream stream = new FileInputStream(file);
                        stream.read(bytes);
                        stream.close();

                        int size = AudioTrack.getMinBufferSize(8000, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
                        track1 = new AudioTrack(AudioManager.STREAM_MUSIC, 8000, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, size, AudioTrack.MODE_STREAM);
                        if (!track1.setPreferredDevice(info)) return;
                        track1.play();
                        track1.write(bytes, 0, bytes.length);
                        track1.release();
                    } catch (IOException exception) {
                        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).start();
            } else if (info.getType() == AudioDeviceInfo.TYPE_USB_HEADSET) {
                new Thread(() -> {
                    try {
                        File file = new File(getFilesDir(), "1.wav");
                        byte[] bytes = new byte[(int) file.length()];

                        FileInputStream stream = new FileInputStream(file);
                        stream.read(bytes);
                        stream.close();

                        int size = AudioTrack.getMinBufferSize(8000, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
                        track2 = new AudioTrack(AudioManager.STREAM_MUSIC, 8000, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, size, AudioTrack.MODE_STREAM);
                        if (!track2.setPreferredDevice(info)) return;
                        track2.play();
                        track2.write(bytes, 0, bytes.length);
                        track2.release();
                    } catch (IOException exception) {
                        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).start();
            }
        }
    }

    public void reflect() {
        try {
            Method method = AudioManager.class.getMethod("setPreferredDeviceForStrategy", Object.class, Object.class);
            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
        } catch (NoSuchMethodException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
