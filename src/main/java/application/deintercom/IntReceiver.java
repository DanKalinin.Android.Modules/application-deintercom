package application.deintercom;

import android.content.Context;
import android.content.Intent;
import library.android.content.LacBroadcastReceiver;

public class IntReceiver extends LacBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        IntContext.shared().onReceive(intent);
    }
}
