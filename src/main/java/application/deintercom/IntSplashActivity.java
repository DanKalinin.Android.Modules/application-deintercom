package application.deintercom;

import android.app.PendingIntent;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import com.google.firebase.messaging.FirebaseMessaging;
import application.deintercom.databinding.IntSplashActivityBinding;
import library.deintercom.IntAsrModel;
import library.deintercom.IntProgrammer;
import library.deintercom.IntPtyFs;

public class IntSplashActivity extends IntActivity implements View.OnClickListener, IntProgrammer.Listener {
    public static final String ACTION_USB_PERMISSION = "action.usb.permission";

    public IntSplashActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntAvrDude.shared().programmer.listeners.add(this);

        binding = IntSplashActivityBinding.inflate(getLayoutInflater());
        binding.error.buttonRetry.setOnClickListener(this);
        setContentView(binding.getRoot());

        mount();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        IntAvrDude.shared().programmer.listeners.remove(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (!ACTION_USB_PERMISSION.equals(intent.getAction())) return;
        UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        boolean granted = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false);
        if (!granted) getToken();
        if (!granted) return;
        UsbManager manager = getSystemService(UsbManager.class);
        UsbDeviceConnection connection = manager.openDevice(device);
        if (connection == null) getToken();
        if (connection == null) return;
        update(connection.getFileDescriptor());
    }

    @Override
    public void onClick(View v) {
        mount();
    }

    @Override
    public void progress(IntProgrammer sender, int percent, double time, String header, int finish) {
        binding.progressIndicator.setIndeterminate(false);
        if (percent < binding.progressIndicator.getProgress()) percent += 100;
        binding.progressIndicator.setProgress(percent);
    }

    public void loading(boolean loading) {
        binding.progressIndicator.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        if (loading) {
            binding.progressIndicator.setVisibility(View.VISIBLE);
        }
    }

    public void success() {
        Intent intent = new Intent(this, IntMainActivity.class);
        startActivity(intent);
        finish();
    }

    public void exception(String text) {
        binding.progressIndicator.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        binding.error.textView.setText(text);
        binding.error.linearLayout.setVisibility(View.VISIBLE);
    }

    public void requestPermission() {
        UsbManager manager = getSystemService(UsbManager.class);

        for (UsbDevice device : manager.getDeviceList().values()) {
            if ((device.getVendorId() != 6790) || (device.getProductId() != 29987)) continue;
            Intent intent = new Intent(this, IntSplashActivity.class);
            intent.setAction(ACTION_USB_PERMISSION);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            manager.requestPermission(device, pendingIntent);
            return;
        }

        getToken();
    }

    public void mount() {
        loading(true);
        IntPtyFs.mountAsync(getFilesDir().toString(), exception -> {
            loading(false);

            if (exception == null) {
                model();
            } else {
                exception(getString(R.string.unknown_error));
            }
        });
    }

    public void model() {
        loading(true);
        IntAsrModel.modelAsync("/data/data/a.intercom/files/vosk-model-small-ru-0.22", (model, exception) -> {
            loading(false);

            if (model == null) {
                exception(getString(R.string.unknown_error));
            } else {
                IntContext.shared().model = model;
                requestPermission();
            }
        });
    }

    public void update(int fd) {
        loading(true);
        IntAvrDude.shared().updateAsync(fd, exception -> {
            loading(false);

            if (exception == null) {
                IntContext.shared().read(fd);
                success();
            } else {
                exception(getString(R.string.unknown_error));
            }
        });
    }

    public void getToken() {
        loading(true);
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            loading(false);

            if (task.isSuccessful()) {
                IntContext.shared().token = task.getResult();
                success();
            } else {
                exception(getString(R.string.check_your_network));
            }
        });
    }
}
