package application.deintercom;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class IntFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(String token) {
        IntContext.shared().token = token;
    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
        String event = message.getData().get("event");

        if (IntContext.EVENT_CALL.equals(event)) {
            String id = message.getData().get("id");
            String a = message.getData().get("a");

            if ("1".equals(a)) {
                IntContext.shared().call(id, (byte) 1);
                String local = message.getData().get("local");
                String external = message.getData().get("external");
                if ((local == null) || (external == null)) return;
                IntContext.shared().ip(id, local, external);
            } else {
                IntContext.shared().call(id, (byte) 0);
            }
        } else if (IntContext.EVENT_IP.equals(event)) {
            String id = message.getData().get("id");
            String local = message.getData().get("local");
            String external = message.getData().get("external");
            IntContext.shared().ip(id, local, external);
        } else if (IntContext.EVENT_ACCEPT.equals(event)) {
            String backendId = message.getData().get("backendId");
            String frontendId = message.getData().get("frontendId");
            String a = message.getData().get("a");

            if ("1".equals(a)) {
                IntContext.shared().accept(backendId, frontendId, (byte) 1);
            } else {
                IntContext.shared().accept(backendId, frontendId, (byte) 0);
            }
        }
    }
}
