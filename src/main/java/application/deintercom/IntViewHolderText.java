package application.deintercom;

import android.view.View;
import android.widget.TextView;
import library.deintercom.IntMessage;

public class IntViewHolderText extends IntViewHolder {
    public TextView textViewBody;

    public IntViewHolderText(View itemView) {
        super(itemView);

        textViewBody = itemView.findViewById(R.id.textViewBody);
    }

    @Override
    public void setMessage(IntMessage message) {
        super.setMessage(message);

        textViewBody.setText(message.text.body);
    }
}
