package application.deintercom;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.slider.Slider;
import java.io.File;
import java.io.IOException;
import library.deintercom.IntMessage;
import library.glibext.GeException;

public class IntViewHolderVoice extends IntViewHolder implements View.OnClickListener, Slider.OnSliderTouchListener, Slider.OnChangeListener, IntPlayer.Listener, IntDownloader.Listener, IntNavigation.Listener {
    public ImageView imageViewPlay;
    public ImageView imageViewPause;
    public TextView textViewPosition;
    public Slider sliderPosition;
    public LinearProgressIndicator indicatorDownload;
    public MaterialCardView cardViewAction;

    public IntViewHolderVoice(View itemView) {
        super(itemView);

        imageViewPlay = itemView.findViewById(R.id.imageViewPlay);
        imageViewPause = itemView.findViewById(R.id.imageViewPause);
        textViewPosition = itemView.findViewById(R.id.textViewPosition);
        sliderPosition = itemView.findViewById(R.id.sliderPosition);
        indicatorDownload = itemView.findViewById(R.id.indicatorDownload);
        cardViewAction = itemView.findViewById(R.id.cardViewAction);

        imageViewPlay.setOnClickListener(this);
        imageViewPause.setOnClickListener(this);
        sliderPosition.addOnSliderTouchListener(this);
        sliderPosition.addOnChangeListener(this);

        IntPlayer.shared().listeners.add(this);
        IntDownloader.shared().listeners.add(this);
        IntNavigation.shared().listeners.add(this);
    }

    @Override
    public void setMessage(IntMessage message) {
        super.setMessage(message);

        String path = itemView.getContext().getFilesDir().toString() + "/" + message.file.backend + "/" + message.file.directory + "/" + message.file.name;
        int visibilityPlay = path.equals(IntPlayer.shared().path) && IntPlayer.shared().isPlaying() || path.equals(IntDownloader.shared().file) ? View.GONE : View.VISIBLE;
        int visibilityPause = path.equals(IntPlayer.shared().path) && IntPlayer.shared().isPlaying() || path.equals(IntDownloader.shared().file) ? View.VISIBLE : View.GONE;
        double position = path.equals(IntPlayer.shared().path) ? message.file.duration - IntPlayer.shared().getCurrentPosition() / 1000.0 : message.file.duration;
        long position1 = Math.round(position);
        long minutes = position1 / 60;
        long seconds = position1 % 60;
        String textPosition = itemView.getContext().getString(R.string.d_02d, minutes, seconds);
        int visibilityPosition = path.equals(IntPlayer.shared().path) ? View.VISIBLE : View.INVISIBLE;
        float valueToPosition = 1000.0F * (float) message.file.duration;
        float valuePosition = path.equals(IntPlayer.shared().path) ? IntPlayer.shared().getCurrentPosition() : 0.0F;
        int visibilityDownload = path.equals(IntPlayer.shared().path) ? View.INVISIBLE : View.VISIBLE;
        boolean indeterminateDownload = path.equals(IntDownloader.shared().file);
        int visibilityAction = (message.call == null) ? View.GONE : View.VISIBLE;
        int colorAction = (message.call == null) ? 0 : IntColor.callActionToColorV1(message.call.action);

        imageViewPlay.setVisibility(visibilityPlay);
        imageViewPause.setVisibility(visibilityPause);
        textViewPosition.setText(textPosition);
        sliderPosition.setVisibility(visibilityPosition);
        sliderPosition.setValueTo(valueToPosition);
        sliderPosition.setValue(valuePosition);
        indicatorDownload.setVisibility(visibilityDownload);
        indicatorDownload.setIndeterminate(indeterminateDownload);
        cardViewAction.setVisibility(visibilityAction);
        cardViewAction.setCardBackgroundColor(colorAction);
    }

    @Override
    public void onClick(View v) {
        try {
            String path = v.getContext().getFilesDir().toString() + "/" + message.file.backend + "/" + message.file.directory + "/" + message.file.name;
            File local = new File(path);

            if (local.exists()) {
                if (v.equals(imageViewPlay)) {
                    if (!path.equals(IntPlayer.shared().path)) {
                        IntDownloader.shared().abort();
                        IntPlayer.shared().reset();
                        IntPlayer.shared().setDataSource(path);
                        IntPlayer.shared().prepare();
                    }

                    IntPlayer.shared().start();
                } else {
                    IntPlayer.shared().pause();
                }
            } else {
                if (v.equals(imageViewPlay)) {
                    IntPlayer.shared().reset();
                    IntDownloader.shared().download(path);
                } else {
                    IntDownloader.shared().abort();
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onStartTrackingTouch(Slider slider) {
        IntPlayer.shared().pause();
    }

    @Override
    public void onStopTrackingTouch(Slider slider) {

    }

    @Override
    public void onValueChange(Slider slider, float value, boolean fromUser) {
        if (!fromUser) return;
        IntPlayer.shared().seekTo((int) value);
    }

    @Override
    public void path(IntPlayer sender) {
        long position = Math.round(message.file.duration);
        long minutes = position / 60;
        long seconds = position % 60;
        String textPosition = itemView.getContext().getString(R.string.d_02d, minutes, seconds);
        String path = itemView.getContext().getFilesDir().toString() + "/" + message.file.backend + "/" + message.file.directory + "/" + message.file.name;
        int visibilityPosition = path.equals(sender.path) ? View.VISIBLE : View.INVISIBLE;
        int visibilityDownload = path.equals(sender.path) ? View.INVISIBLE : View.VISIBLE;

        imageViewPlay.setVisibility(View.VISIBLE);
        imageViewPause.setVisibility(View.GONE);
        textViewPosition.setText(textPosition);
        sliderPosition.setVisibility(visibilityPosition);
        sliderPosition.setValue(0.0F);
        indicatorDownload.setVisibility(visibilityDownload);
    }

    @Override
    public void playing(IntPlayer sender) {
        String path = itemView.getContext().getFilesDir().toString() + "/" + message.file.backend + "/" + message.file.directory + "/" + message.file.name;
        if (!path.equals(sender.path)) return;

        int visibilityPlay = sender.isPlaying() ? View.GONE : View.VISIBLE;
        int visibilityPause = sender.isPlaying() ? View.VISIBLE : View.GONE;

        imageViewPlay.setVisibility(visibilityPlay);
        imageViewPause.setVisibility(visibilityPause);
    }

    @Override
    public void position(IntPlayer sender) {
        String path = itemView.getContext().getFilesDir().toString() + "/" + message.file.backend + "/" + message.file.directory + "/" + message.file.name;
        if (!path.equals(sender.path)) return;

        double position = message.file.duration - sender.getCurrentPosition() / 1000.0;
        long position1 = Math.round(position);
        long minutes = position1 / 60;
        long seconds = position1 % 60;
        String textPosition = itemView.getContext().getString(R.string.d_02d, minutes, seconds);

        textViewPosition.setText(textPosition);
        sliderPosition.setValue(sender.getCurrentPosition());
    }

    @Override
    public void file(IntDownloader sender) {
        String path = itemView.getContext().getFilesDir().toString() + "/" + message.file.backend + "/" + message.file.directory + "/" + message.file.name;
        int visibilityPlay = path.equals(sender.file) ? View.GONE : View.VISIBLE;
        int visibilityPause = path.equals(sender.file) ? View.VISIBLE : View.GONE;
        boolean indeterminateDownload = path.equals(sender.file);

        imageViewPlay.setVisibility(visibilityPlay);
        imageViewPause.setVisibility(visibilityPause);
        indicatorDownload.setVisibility(View.INVISIBLE);
        indicatorDownload.setIndeterminate(indeterminateDownload);
        indicatorDownload.setVisibility(View.VISIBLE);
    }

    @Override
    public void exception(IntDownloader sender, GeException exception) {

    }

    @Override
    public void onPop(IntNavigation sender, IntFragment fragment) {
        if (!(fragment instanceof IntChatFragment)) return;
        IntPlayer.shared().listeners.remove(this);
        IntDownloader.shared().listeners.remove(this);
    }
}
