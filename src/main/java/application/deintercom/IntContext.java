package application.deintercom;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import androidx.core.app.NotificationCompat;
import com.google.firebase.FirebaseApp;
import java.io.File;
import java.util.ArrayList;
import library.apnext.ApnCredential;
import library.asteriskext.AstrDatabase;
import library.asteriskext.AstrSipPeer;
import library.deintercom.IntApnClient;
import library.deintercom.IntAsrModel;
import library.deintercom.IntBackend;
import library.deintercom.IntClient;
import library.deintercom.IntDatabase;
import library.deintercom.IntFrbClient;
import library.deintercom.IntFrontend;
import library.deintercom.IntIgd;
import library.deintercom.IntPinger;
import library.deintercom.IntServer;
import library.deintercom.IntSipAgent;
import library.deintercom.IntUsbSerial;
import library.firebaseext.FrbCredential;
import library.glibext.GeException;
import library.java.lang.LjlObject;
import library.ptyext.PtySession;

public class IntContext extends LjlObject implements IntUsbSerial.Listener, IntUsbSerial.ReadAsyncCallback, IntDatabase.Listener, IntClient.Listener, SharedPreferences.OnSharedPreferenceChangeListener {
    public interface Listener {
        void call(IntContext sender, IntUsbSerial serial, byte a);
        void commit(IntContext sender, IntDatabase database);
        void call(IntContext sender, IntClient client, byte a);
        void ip(IntContext sender, IntClient client, String local, String external);
        void accept(IntContext sender, IntClient client, String id, byte a);
        void progress(IntContext sender, IntClient client, long current, long total, int tag);
        void preferences(IntContext sender, SharedPreferences preferences, String key);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void call(IntContext sender, IntUsbSerial serial, byte a) {
            for (Listener listener : this) {
                listener.call(sender, serial, a);
            }
        }

        @Override
        public void commit(IntContext sender, IntDatabase database) {
            for (Listener listener : this) {
                listener.commit(sender, database);
            }
        }

        @Override
        public void call(IntContext sender, IntClient client, byte a) {
            for (Listener listener : this) {
                listener.call(sender, client, a);
            }
        }

        @Override
        public void ip(IntContext sender, IntClient client, String local, String external) {
            for (Listener listener : this) {
                listener.ip(sender, client, local, external);
            }
        }

        @Override
        public void accept(IntContext sender, IntClient client, String id, byte a) {
            for (Listener listener : this) {
                listener.accept(sender, client, id, a);
            }
        }

        @Override
        public void progress(IntContext sender, IntClient client, long current, long total, int tag) {
            for (Listener listener : this) {
                listener.progress(sender, client, current, total, tag);
            }
        }

        @Override
        public void preferences(IntContext sender, SharedPreferences preferences, String key) {
            for (Listener listener : this) {
                listener.preferences(sender, preferences, key);
            }
        }
    }

    public static final String CHANNEL_INTERCOM = "channel.intercom";
    public static final int NOTIFICATION_SERVICE = 1;
    public static final int NOTIFICATION_CALL = 2;
    public static final String ACTION_FULL_SCREEN = "action.full.screen";
    public static final String ACTION_ACCEPT = "action.accept";
    public static final String ACTION_DECLINE = "action.decline";
    public static final String ACTION_DELETE = "action.delete";
    public static final String PREFERENCES_INTERCOM = "preferences.intercom";
    public static final String KEY_BACKEND = "key.backend";
    public static final String KEY_COLOR = "key.color";
    public static final String EVENT_CALL = "call";
    public static final String EVENT_IP = "ip";
    public static final String EVENT_ACCEPT = "accept";

    private static IntContext ret;
    public Listeners listeners;
    public IntApplication application;
    public IntDatabase databaseClient;
    public IntSipAgent agent;
    public SharedPreferences preferences;
    public IntUsbSerial serial;
    public IntDatabase databaseServer;
    public IntFrbClient frbClient;
    public IntApnClient apnClient;
    public PowerManager.WakeLock wakeLock;
    public WifiManager.WifiLock wifiLock;
    public WifiManager.MulticastLock multicastLock;
    public IntIgd igd;
    public AstrDatabase astrDatabase;
    public PtySession session;
    public int account = -1;
    public IntServer server;
    public IntService service;
    public IntPinger pinger;
    public IntAsrModel model;
    public String token;
    public IntCallActivity callActivity;
    public String callId;

    @Override
    public void call(IntUsbSerial sender, byte a) {
        listeners.call(this, sender, a);
    }

    @Override
    public void commit(IntDatabase sender) {
        listeners.commit(this, sender);
    }

    @Override
    public void call(IntClient sender, byte a) {
        listeners.call(this, sender, a);
    }

    @Override
    public void ip(IntClient sender, String local, String external) {
        listeners.ip(this, sender, local, external);
    }

    @Override
    public void accept(IntClient sender, String id, byte a) {
        listeners.accept(this, sender, id, a);
    }

    @Override
    public void progress(IntClient sender, long current, long total, int tag) {
        listeners.progress(this, sender, current, total, tag);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        listeners.preferences(this, sharedPreferences, key);
    }

    @Override
    public void callback(GeException exception) {
        if (serial != null) serial.close();
        serial = null;
        if (databaseServer != null) databaseServer.close();
        databaseServer = null;
        if (frbClient != null) frbClient.close();
        frbClient = null;
        if (apnClient != null) apnClient.close();
        apnClient = null;
        if (wakeLock != null) wakeLock.release();
        wakeLock = null;
        if (wifiLock != null) wifiLock.release();
        wifiLock = null;
        if (multicastLock != null) multicastLock.release();
        multicastLock = null;
        if (igd != null) igd.close();
        igd = null;
        if (astrDatabase != null) astrDatabase.close();
        astrDatabase = null;
        if (server != null) server.close();
        server = null;
        if (service != null) service.stopSelf();
        service = null;
    }

    public void onReceive(Intent intent) {
        if (intent.getAction().equals(ACTION_DECLINE)) {
            NotificationManager manager = application.getSystemService(NotificationManager.class);
            manager.cancel(NOTIFICATION_CALL);

            callId = null;
        } else if (intent.getAction().equals(ACTION_DELETE)) {
            callId = null;
        }
    }

    public void read(int fd) {
        try {
            if (serial != null) return;
            serial = IntUsbSerial.serial(fd);
            serial.listeners.add(this);
            serial.readAsync(this);
            File fileServer = new File(application.getFilesDir(), "intsqledatabase.server.sqlite3");
            databaseServer = IntDatabase.database(fileServer.toString());
            IntBackend backendServer = new IntBackend();
            backendServer.id = IntCredentials.shared().id;
            databaseServer.backendUpsert(backendServer);
            FrbCredential frbCredential = new FrbCredential();
            frbCredential.privateKeyId = "a6677f2410f9a4ae2a969e8a9e0882cb346fd010";
            frbCredential.privateKey = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDYe4C7pWingn4a\nIUpsvZwBbJEE6iSp1FEllGEVvlUsGmBeGhsgSijI0IaYU02Rz5YdIAUC69IY0OwO\nm0/GKjldF3cPaEsn0aUhqk7K+TDBECfKaYUJLzNFwTCBAQeni+T7VWaQL2pWJsJC\nk8goWXUVSgATtTCV6nLXDs21Q0C2nCu2ILSmD9P5uSCMqFQ7rKGj0lIlji/Fnmv8\nRTCCtzNErKtH13h+TFhvXw2+TDcOnKT1tewyZ1Zg3wtLFqsBNG68JmfzD3j/K0Ur\n7vyGUnowwIA0RPbj+aTmBOEfcOz9nQEFL0SjiHxPcO4fIR2Sk8VprErQqY4GslWl\ncj69pQwfAgMBAAECggEABHN5oKIUhkkxFZF0qcq2P/M/AhZcXZ+PcZHc8ykAMFF0\nhMrNnvrXXtWmxtADqslVYVDsuXQm1DrLd7tqxvzMRCU83fS7gtyVJB2P1C2OepG9\n97P2t8sgJ0GDIUvnn+LI+P/3BWTsL0xJtzu5pFa+iQsF+GHD13lEFjboNN9mn7qJ\nnzZwrDmBVUhtCKrqpRV4V3y9R4ESgzfG7MR2yhS+iRRH+6JhbsjBhOSYsKeoC5Sf\n+IeoQUtGHb8glIFTm5eZyqACON9mv0b3dvfFnKcPIw5AryoGiUi8PKVuE11j3KHX\njfQ6BUkube01ld9wkOdNOfbqexQFCMmWIsfyDyp0qQKBgQDs+IegYYFlVg29tShi\n/SjELKKjo9CZIW70PB5iaJFklK1IC29H97Ph907nhmz/JoPiXZ/hTOyv/B/97+4O\nzcuGOSxJufgDld1wof5x8HQXwlXYxC3k0kuP4LO28IGrR0FpbE0zu10wVn2CTQua\n2b2lUG05O1/lFZc3Fo87dgdsZwKBgQDp3cm8dw7A8Qmm1l6KjyPwSCaDDL19gjf9\nZOL4je9bBbbNi/CtsTXmVvv+2CaEtYeCcikU4TLy88GOCxlj4wIOLhfhEk/cQJt8\nlwDdVjFBo0CfYSMAV4qlhjAQ3Bv0Smja+//+MP0XIxmzPPGdollIa0qSrTmuG6OK\nZdXW5d4PiQKBgAl1gjRsr/WTkcjYylvpLw9lmLhfeZwjMYKeQ7g55FPXe3M8wZxl\nztBeRtAHFaK5rtoCUrdzkp8TuVMxmJcOr+kTryjiO3wpJ0WvsbBqoLeyEiss6YgQ\nvONpXMMYppRnq7zBjfNar0WWrjvdR3FO77sBktqN/C3wxrB0PhNpirJRAoGBAIHC\nkKukd6gwVYWfjgddUriAn/W4vO8FsKFFN/ZncnZTnlFAI0OMfvaWLu4A6qZZvp1v\nUEIIjedlvnBc0jKtAzfw9UjpiywPIPapJk6pA12DAlksYWpQKFqdMNA6ALy0CKfw\niOJ2Ngd/b0QXPYyURuu6tD7cJJPpO/2PcW3yTxrZAoGBAJzjviJz3SOAeaZM4gWt\nQCBkg5Q4Y/ACF7P/ZzC3oo8BtQ8it2IX4CpTz8YnT2RC6QVHurl5U3gG2M+2R9uQ\nTJ2I8EgyDVhr/3YqQCCn9zLtmA0KptXs2fvqRnlH/JwW6RVSLTMj2Zz8v3u/erAm\npXC6OofpWNSuR/3tbPSk/KOA\n-----END PRIVATE KEY-----";
            frbCredential.clientEmail = "firebase-adminsdk-monla@intercom-e57c6.iam.gserviceaccount.com";
            frbClient = IntFrbClient.client("https://fcm.googleapis.com", frbCredential);
            ApnCredential apnCredential = new ApnCredential();
            apnCredential.key = "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgYfABlKXOZFf0E8PN\npjQWvQiq9Ft4aJAC5P6izSosApKgCgYIKoZIzj0DAQehRANCAAQVqG1Ma+IHb3vB\nwtXYcXbPVnO6whi/TPlMX3vWDQNLQwouI5HCFnyay6yOynDImEWjPByofBNmQanA\na/1IJTev\n-----END PRIVATE KEY-----";
            apnCredential.keyId = "TKD5MLALF5";
            apnCredential.teamId = "4W2S6G9WUZ";
            apnClient = IntApnClient.client("https://api.development.push.apple.com", apnCredential);
            PowerManager powerManager = application.getSystemService(PowerManager.class);
            String tag = BuildConfig.APPLICATION_ID + ":" + PowerManager.WakeLock.class.getSimpleName();
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, tag);
            wakeLock.acquire();
            WifiManager wifiManager = application.getSystemService(WifiManager.class);
            wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, BuildConfig.APPLICATION_ID);
            wifiLock.acquire();
            multicastLock = wifiManager.createMulticastLock(BuildConfig.APPLICATION_ID);
            multicastLock.acquire();
            igd = IntIgd.igd("wlan0", 60000, 5060);
            File astrFile = new File(application.getFilesDir(), "usr/var/lib/asterisk/astrsqledatabase.sqlite3");
            astrDatabase = AstrDatabase.database(astrFile.toString());
            AstrSipPeer peer = new AstrSipPeer();
            peer.name = "backend";
            peer.host = "dynamic";
            peer.context = "local";
            peer.secret = IntCredentials.shared().id;
            peer.type = "friend";
            astrDatabase.sipPeerUpsert(peer);
            if (session != null) session.kill();
            if (session != null) session.close();
            session = PtySession.session(BuildConfig.APPLICATION_ID);
            if (account != -1) agent.accountDelete(account);
            account = agent.accountAdd("127.0.0.1", peer.name, peer.secret, false);
            server = IntServer.server(60000, databaseServer, serial, frbClient, apnClient, igd, astrDatabase, agent);
            Intent intentService = new Intent(application, IntService.class);
            application.startForegroundService(intentService);
            IntPlayer.shared().reset();
            IntDownloader.shared().abort();
            if (pinger != null) pinger.close();
            IntClient local = IntClient.client("https://127.0.0.1:60000", IntCredentials.shared().id, backendServer.id, databaseClient);
            local.listeners.add(this);
            pinger = IntPinger.pinger(local, null, local);
            IntFrontend frontend = new IntFrontend();
            frontend.backend = backendServer.id;
            frontend.id = backendServer.id;
            frontend.os = 0;
            frontend.project = FirebaseApp.getInstance().getOptions().getProjectId();
            frontend.token = null;
            IntBackend backendClient = local.registerSync(frontend);
            backendClient.local = "127.0.0.1";
            backendClient.external = null;
            databaseClient.backendUpsert(backendClient);
            local.callsV1Async(null);
            local.filesV1Async("music", null);
            local.passwordsAsync(null);
            local.frontendsAsync(null);
            local.readAsync(null);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(KEY_BACKEND, backendClient.id);
            editor.apply();
        } catch (GeException exception) {
            callback(exception);
        }
    }

    public void call(String id, byte a) {
        try {
            if (a == 1) {
                if (callId != null) return;

                IntBackend[] backends = databaseClient.backendSelectById(id);
                if (backends.length == 0) return;
                IntBackend backend = backends[0];
                String title = (backend.name == null) ? application.getString(R.string.intercom) : backend.name;

                IntPlayer.shared().pause();
                IntDownloader.shared().abort();

                Intent intentFullScreen = new Intent(application, IntCallActivity.class);
                intentFullScreen.setAction(ACTION_FULL_SCREEN);
                intentFullScreen.putExtra("id", id);
                PendingIntent pendingIntentFullScreen = PendingIntent.getActivity(application, 0, intentFullScreen, PendingIntent.FLAG_UPDATE_CURRENT);

                Intent intentAccept = new Intent(application, IntCallActivity.class);
                intentAccept.setAction(ACTION_ACCEPT);
                intentAccept.putExtra("id", id);
                PendingIntent pendingIntentAccept = PendingIntent.getActivity(application, 0, intentAccept, PendingIntent.FLAG_UPDATE_CURRENT);

                Intent intentDecline = new Intent(application, IntReceiver.class);
                intentDecline.setAction(ACTION_DECLINE);
                PendingIntent pendingIntentDecline = PendingIntent.getBroadcast(application, 0, intentDecline, PendingIntent.FLAG_UPDATE_CURRENT);

                Intent intentDelete = new Intent(application, IntReceiver.class);
                intentDelete.setAction(ACTION_DELETE);
                PendingIntent pendingIntentDelete = PendingIntent.getBroadcast(application, 0, intentDelete, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationManager manager = application.getSystemService(NotificationManager.class);

                Notification notification = new NotificationCompat.Builder(application, CHANNEL_INTERCOM)
                        .setSmallIcon(R.drawable.phone)
                        .setContentTitle(title)
                        .setContentText(application.getString(R.string.incoming_call))
                        .setFullScreenIntent(pendingIntentFullScreen, true)
                        .addAction(R.drawable.call, "Accept", pendingIntentAccept)
                        .addAction(R.drawable.call_end, "Decline", pendingIntentDecline)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setCategory(NotificationCompat.CATEGORY_CALL)
                        .setTimeoutAfter(30000)
                        .setDeleteIntent(pendingIntentDelete)
                        .setOngoing(true)
                        .build();

                manager.notify(NOTIFICATION_CALL, notification);

                callId = id;
            } else {
                IntCalls calls = IntCalls.newInstance(id);
                new Handler(Looper.getMainLooper()).postDelayed(calls, 500);

                if ((callId == null) || !callId.equals(id)) return;

                NotificationManager manager = application.getSystemService(NotificationManager.class);
                manager.cancel(NOTIFICATION_CALL);

                callId = null;

                if (callActivity == null) return;

                callActivity.finish();
            }
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    public void ip(String id, String local, String external) {
        try {
            IntBackend[] backends = databaseClient.backendSelectById(id);
            if (backends.length == 0) return;
            IntBackend backend = backends[0];
            backend.local = local;
            backend.external = external;
            databaseClient.backendUpsert(backend);

            if ((pinger == null) || !pinger.current.password.equals(id)) return;

            String baseLocal = "https://" + local + ":60000";
            IntClient clientLocal = IntClient.client(baseLocal, IntCredentials.shared().id, backend.id, databaseClient);
            String baseExternal = "https://" + external + ":60000";
            IntClient clientExternal = IntClient.client(baseExternal, IntCredentials.shared().id, backend.id, databaseClient);
            IntClient clientCurrent = (pinger.current == pinger.local) ? clientLocal : clientExternal;
            pinger.close();
            pinger = IntPinger.pinger(clientLocal, clientExternal, clientCurrent);
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    public void accept(String backendId, String frontendId, byte a) {
        if ((callId == null) || !callId.equals(backendId)) return;

        NotificationManager manager = application.getSystemService(NotificationManager.class);
        manager.cancel(NOTIFICATION_CALL);

        callId = null;

        if (callActivity == null) return;

        callActivity.finish();
    }

    public static IntContext shared() {
        if (ret != null) return ret;
        ret = new IntContext();
        ret.listeners = new Listeners();
        return ret;
    }
}
