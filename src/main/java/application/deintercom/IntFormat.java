package application.deintercom;

import android.media.AudioFormat;
import library.java.lang.LjlObject;

public class IntFormat extends LjlObject {
    public int sampleRate;
    public int channelMask;
    int encoding;

    public static IntFormat newInstance(int sampleRate, int channelMask, int encoding) {
        IntFormat ret = new IntFormat();
        ret.sampleRate = sampleRate;
        ret.channelMask = channelMask;
        ret.encoding = encoding;
        return ret;
    }

    public int getChannelCount() {
        switch (channelMask) {
            case AudioFormat.CHANNEL_IN_MONO: return 1;
            case AudioFormat.CHANNEL_IN_STEREO: return 2;
            default: return 0;
        }
    }

    public int getBytesPerSample() {
        switch (encoding) {
            case AudioFormat.ENCODING_PCM_8BIT: return 1;
            case AudioFormat.ENCODING_PCM_16BIT: return 2;
            case AudioFormat.ENCODING_PCM_24BIT_PACKED: return 3;
            case AudioFormat.ENCODING_PCM_FLOAT: return 4;
            default: return 0;
        }
    }

    public int getBufferSizeInBytes(int duration) {
        return sampleRate * getChannelCount() * getBytesPerSample() * duration / 1000;
    }

    public int getPeriodInFrames(int duration) {
        return sampleRate * duration / 1000;
    }

    public int getBufferSize(int duration) {
        return sampleRate * getChannelCount() * duration / 1000;
    }
}
