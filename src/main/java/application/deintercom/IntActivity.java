package application.deintercom;

import android.os.Bundle;
import library.androidx.appcompat.app.LaaaAppCompatActivity;

public class IntActivity extends LaaaAppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String color = IntContext.shared().preferences.getString(IntContext.KEY_COLOR, null);
        int style = IntColor.colorToStyle(color);
        setTheme(style);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.clear();
    }
}
