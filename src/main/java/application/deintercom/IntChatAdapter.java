package application.deintercom;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import library.deintercom.IntAction;
import library.deintercom.IntMessage;

public class IntChatAdapter extends RecyclerView.Adapter<IntViewHolder> implements IntViewHolderActions.Listener {
    public interface Listener {
        void action(IntChatAdapter sender, IntMessage message, IntAction action);
    }

    public IntMessage[] messages;
    public boolean date;
    public boolean time;
    public Listener listener;

    @Override
    public IntViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(viewType, parent, false);

        if (viewType == R.layout.int_message_text_1) {
            return new IntViewHolderText1(itemView);
        } else if (viewType == R.layout.int_message_text_2) {
            return new IntViewHolderText2(itemView);
        } else if (viewType == R.layout.int_message_call_1) {
            return new IntViewHolderCall1(itemView);
        } else if (viewType == R.layout.int_message_call_2) {
            return new IntViewHolderCall2(itemView);
        } else if (viewType == R.layout.int_message_voice_1) {
            return new IntViewHolderVoice1(itemView);
        } else if (viewType == R.layout.int_message_voice_2) {
            return new IntViewHolderVoice2(itemView);
        } else if (viewType == R.layout.int_message_actions_1) {
            return new IntViewHolderActions1(itemView, this);
        } else {
            return new IntViewHolderActions2(itemView, this);
        }
    }

    @Override
    public void onBindViewHolder(IntViewHolder holder, int position) {
        IntMessage message = messages[position];
        IntMessage previous = position > 0 ? messages[position - 1] : null;
        holder.setMessage(message);
        holder.setPrevious(previous);
        if (!date) holder.textViewDate.setVisibility(View.GONE);
        if (!time) holder.textViewTime.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return messages.length;
    }

    @Override
    public int getItemViewType(int position) {
        IntMessage message = messages[position];

        if (message.actions != null) {
            return message.right ? R.layout.int_message_actions_2 : R.layout.int_message_actions_1;
        } else if (message.file != null) {
            return message.right ? R.layout.int_message_voice_2 : R.layout.int_message_voice_1;
        } else if (message.call != null) {
            return message.right ? R.layout.int_message_call_2 : R.layout.int_message_call_1;
        } else {
            return message.right ? R.layout.int_message_text_2 : R.layout.int_message_text_1;
        }
    }

    @Override
    public long getItemId(int position) {
        IntMessage message = messages[position];
        return message.timestamp;
    }

    @Override
    public void action(IntViewHolderActions sender, IntAction action) {
        listener.action(this, sender.message, action);
    }

    public void setMessages(IntMessage[] messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public static IntChatAdapter newInstance(IntMessage[] messages, boolean date, boolean time) {
        IntChatAdapter ret = new IntChatAdapter();
        ret.setHasStableIds(true);
        ret.messages = messages;
        ret.date = date;
        ret.time = time;
        return ret;
    }
}
