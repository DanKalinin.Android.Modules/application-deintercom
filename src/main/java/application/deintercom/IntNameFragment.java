package application.deintercom;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import application.deintercom.databinding.IntNameFragmentBinding;
import library.androidx.fragment.app.LafaDialogFragment;
import library.deintercom.IntBackend;
import library.glibext.GeException;

public class IntNameFragment extends LafaDialogFragment implements TextWatcher, TextView.OnEditorActionListener, DialogInterface.OnClickListener {
    public IntNameFragmentBinding binding;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        binding = IntNameFragmentBinding.inflate(getLayoutInflater());
        backendSelectById();
        binding.editText.requestFocus();
        binding.editText.addTextChangedListener(this);
        binding.editText.setOnEditorActionListener(this);

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setView(binding.getRoot());
        builder.setPositiveButton(R.string.save, this);
        if (isCancelable()) builder.setNegativeButton(R.string.cancel, this);

        AlertDialog ret = builder.create();
        ret.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        return ret;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        Button button = ((AlertDialog) requireDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
        boolean enabled = s.length() > 0;
        button.setEnabled(enabled);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (v.length() == 0) return true;
        dismiss();
        backendUpsert();
        return true;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which != DialogInterface.BUTTON_POSITIVE) return;
        backendUpsert();
    }

    public void backendSelectById() {
        try {
            IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(IntContext.shared().pinger.current.password);
            if (backends.length == 0) return;
            IntBackend backend = backends[0];
            String text = (backend.name == null) ? getString(R.string.intercom) : backend.name;
            binding.editText.setText(text);
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }

    public void backendUpsert() {
        try {
            IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(IntContext.shared().pinger.current.password);
            if (backends.length == 0) return;
            IntBackend backend = backends[0];
            backend.name = binding.editText.getText().toString();
            IntContext.shared().databaseClient.backendUpsert(backend);
            if (!isCancelable()) IntNavigation.shared().pop((IntFragment) requireParentFragment());
        } catch (GeException exception) {
            exception.printStackTrace();
        }
    }
}
