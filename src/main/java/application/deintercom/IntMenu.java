package application.deintercom;

import library.java.lang.LjlObject;

public class IntMenu extends LjlObject {
    private static IntMenu main;
    public int item;

    public static IntMenu main() {
        if (main != null) return main;
        main = new IntMenu();
        main.item = R.id.menuItemIntercom;
        return main;
    }
}
