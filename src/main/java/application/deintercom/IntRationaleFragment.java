package application.deintercom;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.activity.result.ActivityResultLauncher;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import library.androidx.fragment.app.LafaDialogFragment;

public class IntRationaleFragment extends LafaDialogFragment implements DialogInterface.OnClickListener {
    public ActivityResultLauncher<String> launcher;
    public String permission;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int messageId = getResources().getIdentifier(permission, "string", requireContext().getPackageName());

        return new MaterialAlertDialogBuilder(requireContext())
                .setMessage(messageId)
                .setNegativeButton(R.string.close, this)
                .setPositiveButton(R.string.allow, this)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            launcher.launch(permission);
        }
    }

    public static IntRationaleFragment newInstance(ActivityResultLauncher<String> launcher, String permission) {
        IntRationaleFragment ret = new IntRationaleFragment();
        ret.launcher = launcher;
        ret.permission = permission;
        return ret;
    }
}
