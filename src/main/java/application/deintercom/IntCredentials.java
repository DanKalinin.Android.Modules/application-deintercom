package application.deintercom;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.UUID;
import library.java.lang.LjlObject;

public class IntCredentials extends LjlObject {
    public static final String PREFERENCES_CREDENTIALS = "preferences.credentials";
    public static final String KEY_ID = "key.id";

    private static IntCredentials ret;
    public String id;

    public static IntCredentials shared() {
        if (ret != null) return ret;
        ret = new IntCredentials();
        SharedPreferences preferences = IntContext.shared().application.getSharedPreferences(PREFERENCES_CREDENTIALS, Context.MODE_PRIVATE);
        ret.id = preferences.getString(KEY_ID, null);
        if (ret.id != null) return ret;
        ret.id = UUID.randomUUID().toString();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_ID, ret.id);
        editor.apply();
        return ret;
    }
}
