package application.deintercom;

import android.Manifest;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.android.material.appbar.MaterialToolbar;
import application.deintercom.databinding.IntChatFragmentBinding;
import library.deintercom.IntBackend;
import library.deintercom.IntCall;
import library.deintercom.IntClient;
import library.deintercom.IntDatabase;
import library.deintercom.IntFile;
import library.deintercom.IntMessage;
import library.deintercom.IntText;
import library.deintercom.IntUsbSerial;
import library.glibext.GeException;

public class IntChatFragment extends IntFragment implements MaterialToolbar.OnMenuItemClickListener, View.OnClickListener, IntContext.Listener {
    public IntChatFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding != null) return binding.getRoot();

        IntContext.shared().listeners.add(this);

        IntMessage[] messages = {};
        IntChatAdapter adapter = IntChatAdapter.newInstance(messages, true, true);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());

        binding = IntChatFragmentBinding.inflate(inflater, container, false);
        binding.toolbar.setOnMenuItemClickListener(this);
        binding.toolbar.setNavigationOnClickListener(this);
        binding.chatLayout.binding.recyclerView.setAdapter(adapter);
        binding.chatLayout.binding.recyclerView.setLayoutManager(manager);
        binding.noBackend.buttonAdd.setOnClickListener(this);
        backendSelectById();
        binding.chatLayout.scrollToEnd();
        return binding.getRoot();
    }

    @Override
    public void onPop() {
        IntContext.shared().listeners.remove(this);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.menuItemQrCode) {
            IntQrCodeFragment fragment = new IntQrCodeFragment();
            IntNavigation.shared().push(fragment, IntNavigation.shared().stackChat);
        } else if (item.getItemId() == R.id.menuItemAdd) {
            ((IntMainActivity) requireActivity()).launcher.launch(Manifest.permission.CAMERA);
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.noBackend.buttonAdd)) {
            ((IntMainActivity) requireActivity()).launcher.launch(Manifest.permission.CAMERA);
        } else {
            ((IntMainActivity) requireActivity()).binding.drawerLayout.openDrawer(((IntMainActivity) requireActivity()).binding.navigationView);
        }
    }

    @Override
    public void call(IntContext sender, IntUsbSerial serial, byte a) {

    }

    @Override
    public void commit(IntContext sender, IntDatabase database) {
        backendSelectById();
        if (binding.chatLayout.binding.buttonDown.isOrWillBeShown()) return;
        binding.chatLayout.smoothScrollToEnd();
    }

    @Override
    public void call(IntContext sender, IntClient client, byte a) {

    }

    @Override
    public void ip(IntContext sender, IntClient client, String local, String external) {

    }

    @Override
    public void accept(IntContext sender, IntClient client, String id, byte a) {

    }

    @Override
    public void progress(IntContext sender, IntClient client, long current, long total, int tag) {

    }

    @Override
    public void preferences(IntContext sender, SharedPreferences preferences, String key) {
        if (key.equals(IntContext.KEY_BACKEND)) {
            backendSelectById();
            binding.chatLayout.scrollToEnd();
        }
    }

    public void backendSelectById() {
        try {
            MenuItem itemQrCode = binding.toolbar.getMenu().findItem(R.id.menuItemQrCode);
            MenuItem itemAdd = binding.toolbar.getMenu().findItem(R.id.menuItemAdd);

            itemQrCode.setVisible(false);
            itemAdd.setVisible(false);
            binding.chatLayout.setVisibility(View.GONE);
            binding.noBackend.linearLayout.setVisibility(View.GONE);

            if (IntContext.shared().pinger == null) {
                itemAdd.setVisible(true);
                binding.noBackend.linearLayout.setVisibility(View.VISIBLE);
            } else {
                IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(IntContext.shared().pinger.current.password);
                if (backends.length == 0) return;
                IntBackend backend = backends[0];

                IntCall[] calls = IntContext.shared().databaseClient.callSelectByBackend(backend.id, "ORDER BY started");
                IntMessage[] messages = messages(calls);
                ((IntChatAdapter) binding.chatLayout.binding.recyclerView.getAdapter()).setMessages(messages);

                boolean visibleQrCode = IntContext.shared().pinger.external == null;

                itemQrCode.setVisible(visibleQrCode);
                itemAdd.setVisible(true);
                binding.chatLayout.setVisibility(View.VISIBLE);
            }
        } catch (GeException exception) {
            Toast.makeText(getContext(), exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public IntMessage[] messages(IntCall[] calls) throws GeException {
        IntMessage[] ret = null;

        if (calls.length > 0) {
            ret = new IntMessage[calls.length];

            for (int i = 0; i < calls.length; i++) {
                IntCall call = calls[i];
                ret[i] = new IntMessage();
                ret[i].timestamp = call.started;
                ret[i].call = call;
                long ended = call.ended + 1000;
                IntFile[] files = IntContext.shared().databaseClient.fileSelectByBackendDirectoryModifiedBetween(call.backend, "music", call.started, ended, null);
                if (files.length > 0) ret[i].file = files[0];
            }
        } else {
            ret = new IntMessage[2];
            ret[0] = new IntMessage();
            ret[0].timestamp = System.currentTimeMillis();
            ret[0].text = new IntText();
            ret[0].text.body = getString(R.string.body_1);
            ret[1] = new IntMessage();
            ret[1].timestamp = ret[0].timestamp + 1;
            ret[1].text = new IntText();
            ret[1].text.body = getString(R.string.body_2);
        }

        return ret;
    }
}
