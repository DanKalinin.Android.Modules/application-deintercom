package application.deintercom;

import androidx.fragment.app.FragmentManager;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Stack;
import library.java.lang.LjlObject;

public class IntNavigation extends LjlObject implements Closeable {
    public interface Listener {
        void onPop(IntNavigation sender, IntFragment fragment);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void onPop(IntNavigation sender, IntFragment fragment) {
            fragment.onPop();

            for (Listener listener : this) {
                listener.onPop(sender, fragment);
            }
        }
    }

    private static IntNavigation ret;
    public Listeners listeners;
    public Stack<IntFragment> stackChat;
    public Stack<IntFragment> stackSettings;
    public Stack<IntFragment> stackCurrent;
    public FragmentManager fragmentManager;

    @Override
    public void close() {
        while (!stackChat.empty()) listeners.onPop(this, stackChat.pop());
        while (!stackSettings.empty()) listeners.onPop(this, stackSettings.pop());
        ret = null;
    }

    public void set(Stack<IntFragment> stack) {
        if (stack.equals(stackCurrent)) while (stack.size() > 1) listeners.onPop(this, stack.pop());
        stackCurrent = stack;
        fragmentManager.beginTransaction().replace(R.id.frameLayout, stack.peek()).commit();
    }

    boolean back() {
        if (stackCurrent.size() == 1) return false;
        listeners.onPop(this, stackCurrent.pop());
        fragmentManager.beginTransaction().replace(R.id.frameLayout, stackCurrent.peek()).commit();
        return true;
    }

    public void push(IntFragment fragment, Stack<IntFragment> stack) {
        stack.push(fragment);
        fragment.stack = stack;
        if (!stack.equals(stackCurrent)) return;
        fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).commit();
    }

    public void pop(IntFragment fragment) {
        while (!fragment.stack.peek().equals(fragment)) listeners.onPop(this, fragment.stack.pop());
        listeners.onPop(this, fragment.stack.pop());
        if (!fragment.stack.equals(stackCurrent)) return;
        fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment.stack.peek()).commit();
    }

    public static IntNavigation shared() {
        if (ret != null) return ret;
        ret = new IntNavigation();
        ret.listeners = new Listeners();
        ret.stackChat = new Stack<>();
        IntChatFragment chatFragment = new IntChatFragment();
        ret.stackChat.push(chatFragment);
        ret.stackSettings = new Stack<>();
        IntSettingsFragment settingsFragment = new IntSettingsFragment();
        ret.stackSettings.push(settingsFragment);
        return ret;
    }
}
