package application.deintercom;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import library.androidx.fragment.app.LafaDialogFragment;

public class IntDeniedFragment extends LafaDialogFragment implements DialogInterface.OnClickListener {
    public String permission;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int messageId = getResources().getIdentifier(permission, "string", requireContext().getPackageName());

        return new MaterialAlertDialogBuilder(requireContext())
                .setMessage(messageId)
                .setNegativeButton(R.string.close, this)
                .setPositiveButton(R.string.allow_in_settings, this)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            Uri uri = Uri.fromParts("package", requireContext().getPackageName(), null);
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri);
            startActivity(intent);
        }
    }

    public static IntDeniedFragment newInstance(String permission) {
        IntDeniedFragment ret = new IntDeniedFragment();
        ret.permission = permission;
        return ret;
    }
}
