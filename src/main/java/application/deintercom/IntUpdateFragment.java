package application.deintercom;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.io.File;
import application.deintercom.databinding.IntUpdateFragmentBinding;
import library.fdroidext.FdrApplication;
import library.fdroidext.FdrClient;
import library.fdroidext.FdrIndex;
import library.fdroidext.FdrPackage;

public class IntUpdateFragment extends IntFragment implements View.OnClickListener, FdrClient.Listener {
    public IntUpdateFragmentBinding binding;
    public String name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding != null) return binding.getRoot();

        IntFdroid.shared().client.listeners.add(this);

        binding = IntUpdateFragmentBinding.inflate(inflater, container, false);
        binding.toolbar.setNavigationOnClickListener(this);
        binding.buttonUpdate.setOnClickListener(this);
        binding.buttonStop.setOnClickListener(this);
        binding.error.buttonRetry.setOnClickListener(this);
        index();
        return binding.getRoot();
    }

    @Override
    public void onPop() {
        IntFdroid.shared().client.listeners.remove(this);
        IntFdroid.shared().abort();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.buttonUpdate)) {
            download();
        } else if (v.equals(binding.buttonStop)) {
            IntFdroid.shared().abort();
        } else if (v.equals(binding.error.buttonRetry)) {
            index();
        } else {
            IntNavigation.shared().pop(this);
        }
    }

    @Override
    public void progress(FdrClient sender, long current, long total, int tag) {
        long progress = 100 * current / total;
        if (progress < 1) return;
        binding.indicatorUpdate.setIndeterminate(false);
        binding.indicatorUpdate.setProgress((int) progress);
    }

    public void loading(boolean loading) {
        binding.scrollViewUpdate.setVisibility(View.GONE);
        binding.indicatorMain.setVisibility(View.GONE);
        binding.success.linearLayout.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        if (loading) {
            binding.indicatorMain.setVisibility(View.VISIBLE);
        }
    }

    public void success(FdrIndex index) {
        binding.scrollViewUpdate.setVisibility(View.GONE);
        binding.indicatorMain.setVisibility(View.GONE);
        binding.success.linearLayout.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        for (FdrApplication application : index.applications) {
            if (!BuildConfig.APPLICATION_ID.equals(application.id)) continue;

            for (FdrPackage package1 : application.packages) {
                if (package1.versioncode <= BuildConfig.VERSION_CODE) continue;
                name = package1.apkname;
                String version = getString(R.string.version_s, package1.version);
                binding.textViewName.setText(application.name);
                binding.textViewVersion.setText(version);
                binding.textViewChangelog.setText(application.desc);
                binding.scrollViewUpdate.setVisibility(View.VISIBLE);
                return;
            }
        }

        binding.success.textView.setText(R.string.application_is_up_to_date);
        binding.success.linearLayout.setVisibility(View.VISIBLE);
    }

    public void exception(String text) {
        binding.scrollViewUpdate.setVisibility(View.GONE);
        binding.indicatorMain.setVisibility(View.GONE);
        binding.success.linearLayout.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        binding.error.textView.setText(text);
        binding.error.linearLayout.setVisibility(View.VISIBLE);
    }

    public void index() {
        if (!IntFdroid.shared().delete()) return;

        loading(true);
        IntFdroid.shared().indexAsync((index, exception) -> {
            loading(false);

            if (index == null) {
                exception(getString(R.string.unable_to_check_for_update));
            } else {
                success(index);
            }
        });
    }

    public void loadingV1(boolean loading) {
        binding.buttonUpdate.setVisibility(View.GONE);
        binding.layoutStop.setVisibility(View.GONE);

        if (loading) {
            binding.indicatorUpdate.setIndeterminate(true);
            binding.layoutStop.setVisibility(View.VISIBLE);
        } else {
            binding.buttonUpdate.setVisibility(View.VISIBLE);
        }
    }

    public void successV1(File file) {
        Uri uri = IntFileProvider.getUriForFile(requireContext(), IntFileProvider.class.getName(), file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    public void exceptionV1(String text) {
        IntErrorFragment fragment = IntErrorFragment.newInstance(text);
        fragment.show(getChildFragmentManager(), null);
    }

    public void download() {
        IntFdroid.shared().deleteAsync(null);

        loadingV1(true);
        IntFdroid.shared().downloadAsync(name, (file, exception) -> {
            loadingV1(false);

            if (file == null) {
                if (exception.code == 1) return;
                exceptionV1(getString(R.string.unable_to_download_update));
            } else {
                successV1(file);
            }
        });
    }
}
