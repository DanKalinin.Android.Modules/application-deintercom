package application.deintercom;

import android.view.LayoutInflater;
import android.view.View;
import com.google.android.material.chip.ChipGroup;
import library.deintercom.IntAction;
import library.deintercom.IntMessage;

public class IntViewHolderActions extends IntViewHolder implements View.OnClickListener {
    public interface Listener {
        void action(IntViewHolderActions sender, IntAction action);
    }

    public Listener listener;
    public ChipGroup chipGroup;

    public IntViewHolderActions(View itemView, Listener listener) {
        super(itemView);

        this.listener = listener;

        chipGroup = itemView.findViewById(R.id.chipGroup);
    }

    @Override
    public void setMessage(IntMessage message) {
        super.setMessage(message);

        chipGroup.removeAllViews();

        for (IntAction action : message.actions) {
            LayoutInflater inflater = LayoutInflater.from(itemView.getContext());
            IntActionChip chip = (IntActionChip) inflater.inflate(R.layout.int_action_chip, chipGroup, false);
            chip.setOnClickListener(this);
            chip.setAction(action);
            chipGroup.addView(chip);
        }
    }

    @Override
    public void onClick(View v) {
        listener.action(this, ((IntActionChip) v).action);
    }
}
