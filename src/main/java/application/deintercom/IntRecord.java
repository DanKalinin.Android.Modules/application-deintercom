package application.deintercom;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import java.util.ArrayList;
import library.android.media.LamAudioRecord;

public class IntRecord extends LamAudioRecord implements AudioRecord.OnRecordPositionUpdateListener {
    public interface Listener {
        void onStop(IntRecord sender);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void onStop(IntRecord sender) {
            for (Listener listener : this) {
                listener.onStop(sender);
            }
        }
    }

    private static IntRecord ret;
    public Listeners listeners;
    public short[] audioData;

    public IntRecord(int audioSource, int sampleRateInHz, int channelConfig, int audioFormat, int bufferSizeInBytes) throws IllegalArgumentException {
        super(audioSource, sampleRateInHz, channelConfig, audioFormat, bufferSizeInBytes);
    }

    @Override
    public void onMarkerReached(AudioRecord recorder) {
        stop();
        listeners.onStop(this);
    }

    @Override
    public void onPeriodicNotification(AudioRecord recorder) {

    }

    public static IntRecord shared() {
        if (ret != null) return ret;
        IntFormat format = IntFormat.newInstance(16000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        int duration = 10000;
        int bufferSizeInBytes = format.getBufferSizeInBytes(duration) + getMinBufferSize(format.sampleRate, format.channelMask, format.encoding);
        int periodInFrames = format.getPeriodInFrames(duration);
        int bufferSize = format.getBufferSize(duration);
        ret = new IntRecord(MediaRecorder.AudioSource.MIC, format.sampleRate, format.channelMask, format.encoding, bufferSizeInBytes);
        ret.setNotificationMarkerPosition(periodInFrames);
        ret.setPositionNotificationPeriod(16000);
        ret.setRecordPositionUpdateListener(ret);
        ret.listeners = new Listeners();
        ret.audioData = new short[bufferSize];
        return ret;
    }
}
