package application.deintercom;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatDelegate;
import java.io.File;
import library.android.app.LaaApplication;
import library.deintercom.IntDatabase;
import library.deintercom.IntSipAgent;
import library.deintercom.IntTone;
import library.glibext.GeException;

public class IntApplication extends LaaApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        try {
            IntContext.shared().application = this;

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            NotificationManager manager = getSystemService(NotificationManager.class);

            NotificationChannel channel = new NotificationChannel(IntContext.CHANNEL_INTERCOM, getString(R.string.intercom), NotificationManager.IMPORTANCE_HIGH);
            channel.setSound(sound, attributes);
            manager.createNotificationChannel(channel);

            File fileClient = new File(getFilesDir(), "intsqledatabase.client.sqlite3");
            IntContext.shared().databaseClient = IntDatabase.database(fileClient.toString());
            IntContext.shared().databaseClient.listeners.add(IntContext.shared());

            File ring = new File(getFilesDir(), "intring.wav");
            File busy = new File(getFilesDir(), "intbusy.wav");
            IntTone tone = new IntTone();
            tone.ring = ring.toString();
            tone.busy = busy.toString();
            IntContext.shared().agent = IntSipAgent.agent(null, tone);

            IntContext.shared().preferences = getSharedPreferences(IntContext.PREFERENCES_INTERCOM, MODE_PRIVATE);
            IntContext.shared().preferences.registerOnSharedPreferenceChangeListener(IntContext.shared());
        } catch (GeException exception) {
            Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
