package application.deintercom;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import application.deintercom.databinding.IntSettingsFragmentBinding;
import library.deintercom.IntBackend;
import library.deintercom.IntClient;
import library.deintercom.IntDatabase;
import library.deintercom.IntPassword;
import library.deintercom.IntUsbSerial;
import library.glibext.GeException;

public class IntSettingsFragment extends IntFragment implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, CompoundButton.OnCheckedChangeListener, DialogInterface.OnClickListener, IntContext.Listener {
    public IntSettingsFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding != null) return binding.getRoot();

        IntContext.shared().listeners.add(this);

        binding = IntSettingsFragmentBinding.inflate(inflater, container, false);
        binding.toolbar.setNavigationOnClickListener(this);
        binding.navigationView.setNavigationItemSelectedListener(this);
        backendSelectById();
        return binding.getRoot();
    }

    @Override
    public void onPop() {
        IntContext.shared().listeners.remove(this);
    }

    @Override
    public void onClick(View v) {
        ((IntMainActivity) requireActivity()).binding.drawerLayout.openDrawer(((IntMainActivity) requireActivity()).binding.navigationView);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuItemAdd) {
            ((IntMainActivity) requireActivity()).launcher.launch(Manifest.permission.CAMERA);
        } else if (item.getItemId() == R.id.menuItemName) {
            IntNameFragment fragment = new IntNameFragment();
            fragment.show(getChildFragmentManager(), null);
        } else if (item.getItemId() == R.id.menuItemRingtone) {
            Intent intent = new Intent(Settings.ACTION_SOUND_SETTINGS);
            startActivity(intent);
        } else if (item.getItemId() == R.id.menuItemColor) {
            IntColorFragment fragment = new IntColorFragment();
            fragment.show(getChildFragmentManager(), null);
        } else if (item.getItemId() == R.id.menuItemWifi) {
            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            startActivity(intent);
        } else if (item.getItemId() == R.id.menuItemLanguage) {
            Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(intent);
        } else if (item.getItemId() == R.id.menuItemDateAndTime) {
            Intent intent = new Intent(Settings.ACTION_DATE_SETTINGS);
            startActivity(intent);
        } else if (item.getItemId() == R.id.menuItemUpdate) {
            IntUpdateFragment fragment = new IntUpdateFragment();
            IntNavigation.shared().push(fragment, IntNavigation.shared().stackSettings);
        } else if (item.getItemId() == R.id.menuItemPassword) {
            IntPasswordFragment fragment = new IntPasswordFragment();
            IntNavigation.shared().push(fragment, IntNavigation.shared().stackSettings);
        } else if (item.getItemId() == R.id.menuItemQrCode) {
            IntQrCodeFragment fragment = new IntQrCodeFragment();
            IntNavigation.shared().push(fragment, IntNavigation.shared().stackSettings);
        } else if (item.getItemId() == R.id.menuItemDelete) {
            new MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.are_you_sure)
                    .setNegativeButton(R.string.cancel, this)
                    .setPositiveButton(R.string.delete, this)
                    .show();
        }

        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try {
            if (!buttonView.isPressed()) return;
            IntPassword[] passwords = IntContext.shared().databaseClient.passwordSelectByBackendName(IntContext.shared().pinger.current.password, "password.m4a");

            if (passwords.length == 0) {
                IntPasswordFragment fragment = new IntPasswordFragment();
                IntNavigation.shared().push(fragment, IntNavigation.shared().stackSettings);
                buttonView.setChecked(false);
            } else {
                passwordEnable(isChecked);
            }
        } catch (GeException exception) {
            exception(getString(R.string.unknown_error));
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            loading(true);
            IntContext.shared().pinger.current.unregisterAsync(exception -> {
                try {
                    loading(false);

                    IntContext.shared().databaseClient.backendDeleteById(IntContext.shared().pinger.current.password);

                    IntContext.shared().pinger.close();
                    IntContext.shared().pinger = null;

                    SharedPreferences.Editor editor = IntContext.shared().preferences.edit();
                    editor.remove(IntContext.KEY_BACKEND);
                    editor.apply();

                    success();
                } catch (GeException exception1) {
                    exception(getString(R.string.unknown_error));
                }
            });
        }
    }

    @Override
    public void call(IntContext sender, IntUsbSerial serial, byte a) {

    }

    @Override
    public void commit(IntContext sender, IntDatabase database) {
        backendSelectById();
    }

    @Override
    public void call(IntContext sender, IntClient client, byte a) {

    }

    @Override
    public void ip(IntContext sender, IntClient client, String local, String external) {

    }

    @Override
    public void accept(IntContext sender, IntClient client, String id, byte a) {

    }

    @Override
    public void progress(IntContext sender, IntClient client, long current, long total, int tag) {

    }

    @Override
    public void preferences(IntContext sender, SharedPreferences preferences, String key) {
        if (key.equals(IntContext.KEY_BACKEND)) {
            backendSelectById();
        }
    }

    public void loading(boolean loading) {
        binding.progressIndicator.setVisibility(View.GONE);
        requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        if (loading) {
            binding.progressIndicator.setVisibility(View.VISIBLE);
            requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public void success() {

    }

    public void exception(String text) {
        IntErrorFragment fragment = IntErrorFragment.newInstance(text);
        fragment.show(getChildFragmentManager(), null);
    }

    public void backendSelectById() {
        try {
            MenuItem itemAdd = binding.navigationView.getMenu().findItem(R.id.menuItemAdd);
            MenuItem itemName = binding.navigationView.getMenu().findItem(R.id.menuItemName);
            MenuItem itemRingtone = binding.navigationView.getMenu().findItem(R.id.menuItemRingtone);
            MenuItem itemColor = binding.navigationView.getMenu().findItem(R.id.menuItemColor);
            MenuItem itemWifi = binding.navigationView.getMenu().findItem(R.id.menuItemWifi);
            MenuItem itemLanguage = binding.navigationView.getMenu().findItem(R.id.menuItemLanguage);
            MenuItem itemDateAndTime = binding.navigationView.getMenu().findItem(R.id.menuItemDateAndTime);
            MenuItem itemUpdate = binding.navigationView.getMenu().findItem(R.id.menuItemUpdate);
            MenuItem itemPassword = binding.navigationView.getMenu().findItem(R.id.menuItemPassword);
            MenuItem itemQrCode = binding.navigationView.getMenu().findItem(R.id.menuItemQrCode);
            MenuItem itemDelete = binding.navigationView.getMenu().findItem(R.id.menuItemDelete);

            itemAdd.setVisible(false);
            itemName.setVisible(false);
            itemRingtone.setVisible(false);
            itemColor.setVisible(false);
            itemWifi.setVisible(false);
            itemLanguage.setVisible(false);
            itemDateAndTime.setVisible(false);
            itemUpdate.setVisible(false);
            itemQrCode.setVisible(false);
            itemDelete.setVisible(false);

            ((CompoundButton) itemPassword.getActionView()).setOnCheckedChangeListener(this);
            ((CompoundButton) itemPassword.getActionView()).setChecked(false);

            if (IntContext.shared().pinger == null) {
                itemAdd.setVisible(true);
                itemColor.setVisible(true);
                itemWifi.setVisible(true);
                itemLanguage.setVisible(true);
                itemDateAndTime.setVisible(true);
                itemUpdate.setVisible(true);
            } else {
                IntBackend[] backends = IntContext.shared().databaseClient.backendSelectById(IntContext.shared().pinger.current.password);
                if (backends.length == 0) return;
                IntBackend backend = backends[0];

                Intent intentRingtone = new Intent(Settings.ACTION_SOUND_SETTINGS);
                Intent intentWifi = new Intent(Settings.ACTION_WIFI_SETTINGS);
                Intent intentLanguage = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                Intent intentDateAndTime = new Intent(Settings.ACTION_DATE_SETTINGS);

                boolean visibleRingtone = intentRingtone.resolveActivity(IntContext.shared().application.getPackageManager()) != null;
                boolean visibleWifi = intentWifi.resolveActivity(IntContext.shared().application.getPackageManager()) != null;
                boolean visibleLanguage = intentLanguage.resolveActivity(IntContext.shared().application.getPackageManager()) != null;
                boolean visibleDateAndTime = intentDateAndTime.resolveActivity(IntContext.shared().application.getPackageManager()) != null;
                boolean visibleQrCode = IntContext.shared().pinger.external == null;
                boolean visibleDelete = IntContext.shared().pinger.external != null;

                itemName.setVisible(true);
                itemRingtone.setVisible(visibleRingtone);
                itemColor.setVisible(true);
                itemWifi.setVisible(visibleWifi);
                itemLanguage.setVisible(visibleLanguage);
                itemDateAndTime.setVisible(visibleDateAndTime);
                itemUpdate.setVisible(true);
                itemQrCode.setVisible(visibleQrCode);
                itemDelete.setVisible(visibleDelete);

                IntPassword[] passwords = IntContext.shared().databaseClient.passwordSelectByBackendName(IntContext.shared().pinger.current.password, "password.m4a");
                if (passwords.length == 0) return;
                IntPassword password = passwords[0];

                ((CompoundButton) itemPassword.getActionView()).setChecked(password.enabled);
            }
        } catch (GeException exception) {
            exception(getString(R.string.unknown_error));
        }
    }

    public void passwordEnable(boolean enabled) {
        loading(true);
        IntContext.shared().pinger.current.passwordEnableAsync("password.m4a", enabled, exception -> {
            loading(false);

            if (exception == null) {
                success();
            } else {
                exception(exception.getLocalizedMessage());
            }
        });
    }
}
