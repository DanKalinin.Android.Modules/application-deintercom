package application.deintercom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import application.deintercom.databinding.IntChatLayoutBinding;
import library.android.widget.LawFrameLayout;

public class IntChatLayout extends LawFrameLayout implements View.OnScrollChangeListener, View.OnClickListener {
    public IntChatLayoutBinding binding;

    public IntChatLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = LayoutInflater.from(context);
        binding = IntChatLayoutBinding.inflate(inflater, this);
        binding.recyclerView.setOnScrollChangeListener(this);
        binding.buttonDown.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        smoothScrollToEnd();
    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        int offset = binding.recyclerView.computeVerticalScrollRange() - binding.recyclerView.computeVerticalScrollExtent() - binding.recyclerView.computeVerticalScrollOffset();

        if (offset > 200) {
            binding.buttonDown.show();
        } else {
            binding.buttonDown.hide();
        }
    }

    public void scrollToEnd() {
        int position = binding.recyclerView.getAdapter().getItemCount() - 1;
        binding.recyclerView.scrollToPosition(position);
    }

    public void smoothScrollToEnd() {
        int position = binding.recyclerView.getAdapter().getItemCount() - 1;
        binding.recyclerView.smoothScrollToPosition(position);
    }
}
