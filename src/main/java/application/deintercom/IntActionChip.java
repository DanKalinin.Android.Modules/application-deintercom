package application.deintercom;

import android.content.Context;
import android.util.AttributeSet;
import library.com.google.android.material.chip.LcgamcChip;
import library.deintercom.IntAction;

public class IntActionChip extends LcgamcChip {
    public IntAction action;

    public IntActionChip(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setAction(IntAction action) {
        this.action = action;

        setText(action.text);
    }
}
