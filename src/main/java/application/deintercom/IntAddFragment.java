package application.deintercom;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.firebase.FirebaseApp;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.ScanOptions;
import application.deintercom.databinding.IntAddFragmentBinding;
import library.deintercom.IntBackend;
import library.deintercom.IntClient;
import library.deintercom.IntFrontend;
import library.deintercom.IntJwt;
import library.deintercom.IntPinger;
import library.deintercom.IntQrCode;
import library.glibext.GeException;

public class IntAddFragment extends IntFragment implements View.OnClickListener, BarcodeCallback {
    public IntAddFragmentBinding binding;
    public BeepManager beepManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding != null) return binding.getRoot();

        beepManager = new BeepManager(requireActivity());
        beepManager.setBeepEnabled(false);
        beepManager.setVibrateEnabled(true);

        ScanOptions options = new ScanOptions();
        options.setDesiredBarcodeFormats(ScanOptions.QR_CODE);
        options.setPrompt("");

        Intent intent = options.createScanIntent(getContext());

        binding = IntAddFragmentBinding.inflate(inflater, container, false);
        binding.toolbar.setNavigationOnClickListener(this);
        binding.barcodeView.initializeFromIntent(intent);
        binding.error.buttonRetry.setOnClickListener(this);
        initial();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        binding.barcodeView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();

        binding.barcodeView.pauseAndWait();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.error.buttonRetry)) {
            initial();
        } else {
            IntNavigation.shared().pop(this);
        }
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        try {
            IntQrCode qrCode = IntJwt.qrCodeDecode(result.getText(), BuildConfig.APPLICATION_ID);
            beepManager.playBeepSoundAndVibrate();
            register(qrCode);
        } catch (GeException exception) {
            exception(getString(R.string.invalid_qr_code));
        }
    }

    public void initial() {
        binding.frameLayoutQr.setVisibility(View.GONE);
        binding.progressIndicator.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        binding.barcodeView.decodeSingle(this);
        binding.frameLayoutQr.setVisibility(View.VISIBLE);
    }

    public void loading(boolean loading) {
        binding.frameLayoutQr.setVisibility(View.GONE);
        binding.progressIndicator.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        if (loading) {
            binding.progressIndicator.setVisibility(View.VISIBLE);
        }
    }

    public void success(IntBackend backend) {
        binding.frameLayoutQr.setVisibility(View.GONE);
        binding.progressIndicator.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        IntNameFragment fragment = new IntNameFragment();
        fragment.setCancelable(false);
        fragment.show(getChildFragmentManager(), null);
    }

    public void exception(String text) {
        binding.frameLayoutQr.setVisibility(View.GONE);
        binding.progressIndicator.setVisibility(View.GONE);
        binding.error.linearLayout.setVisibility(View.GONE);

        binding.error.textView.setText(text);
        binding.error.linearLayout.setVisibility(View.VISIBLE);
    }

    public void register(IntQrCode qrCode) {
        String baseLocal = "https://" + qrCode.local + ":60000";
        IntClient local = IntClient.client(baseLocal, IntCredentials.shared().id, qrCode.password, IntContext.shared().databaseClient);

        IntFrontend frontend = new IntFrontend();
        frontend.backend = qrCode.password;
        frontend.id = IntCredentials.shared().id;
        frontend.os = 0;
        frontend.project = FirebaseApp.getInstance().getOptions().getProjectId();
        frontend.token = IntContext.shared().token;

        loading(true);
        local.registerAsync(frontend, (backend, exception) -> {
            try {
                loading(false);

                if (backend == null) {
                    exception(getString(R.string.connect_the_device_to_your_home_network));
                } else {
                    backend.local = qrCode.local;
                    backend.external = qrCode.external;
                    IntContext.shared().databaseClient.backendUpsert(backend);

                    IntPlayer.shared().reset();
                    IntDownloader.shared().abort();
                    if (IntContext.shared().pinger != null) IntContext.shared().pinger.close();
                    String baseExternal = "https://" + qrCode.external + ":60000";
                    IntClient external = IntClient.client(baseExternal, IntCredentials.shared().id, qrCode.password, IntContext.shared().databaseClient);
                    IntContext.shared().pinger = IntPinger.pinger(local, external, local);
                    local.callsV1Async(null);
                    local.filesV1Async("music", null);
                    local.passwordsAsync(null);
                    local.frontendsAsync(null);

                    SharedPreferences.Editor editor = IntContext.shared().preferences.edit();
                    editor.putString(IntContext.KEY_BACKEND, backend.id);
                    editor.apply();

                    success(backend);
                }
            } catch (GeException exception1) {
                exception(getString(R.string.unknown_error));
            }
        });
    }
}
