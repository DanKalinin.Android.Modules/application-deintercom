package application.deintercom;

import android.os.Handler;
import android.os.Looper;
import java.io.File;
import library.deintercom.IntProgrammer;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntAvrDude extends LjlObject {
    private static IntAvrDude ret;
    public IntProgrammer programmer;

    public static IntAvrDude shared() {
        if (ret != null) return ret;
        ret = new IntAvrDude();
        ret.programmer = IntProgrammer.programmer();
        return ret;
    }

    // Update

    public boolean updateSync(int fd) throws GeException {
        File config = new File(IntContext.shared().application.getFilesDir(), "avdavrdude.conf");
        File firmware = new File(IntContext.shared().application.getFilesDir(), "intfirmware.hex");
        String port = "" + fd;
        return programmer.updateSync(config.toString(), firmware.toString(), port);
    }

    public interface UpdateAsyncCallback {
        void callback(GeException exception);
    }

    public void updateAsync(int fd, UpdateAsyncCallback callback) {
        new Thread(() -> {
            try {
                updateSync(fd);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }
}
