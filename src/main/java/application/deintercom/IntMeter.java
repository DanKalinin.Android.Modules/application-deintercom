package application.deintercom;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import java.util.ArrayList;
import library.android.media.LamAudioRecord;

public class IntMeter extends LamAudioRecord implements AudioRecord.OnRecordPositionUpdateListener {
    public interface Listener {
        void onAmplitude(IntMeter sender, float amplitude);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void onAmplitude(IntMeter sender, float amplitude) {
            for (Listener listener : this) {
                listener.onAmplitude(sender, amplitude);
            }
        }
    }

    public static final int N = 10;
    public static final float K = 8.0f;

    private static IntMeter ret;
    public Listeners listeners;
    public float[] audioData;

    public IntMeter(int audioSource, int sampleRateInHz, int channelConfig, int audioFormat, int bufferSizeInBytes) throws IllegalArgumentException {
        super(audioSource, sampleRateInHz, channelConfig, audioFormat, bufferSizeInBytes);
    }

    @Override
    public void onMarkerReached(AudioRecord recorder) {

    }

    @Override
    public void onPeriodicNotification(AudioRecord recorder) {
        int n = read(audioData, 0, audioData.length, READ_NON_BLOCKING);
        if (n < N) return;
        int di = n / N;
        float max = 0.0f;

        for (int i = 0; i < n; i += di) {
            float amplitude = audioData[i];
            amplitude = K * Math.abs(amplitude);
            max = Math.max(max, amplitude);
        }

        max = Math.min(max, 1.0f);
        listeners.onAmplitude(this, max);
    }

    public static IntMeter shared() {
        if (ret != null) return ret;
        IntFormat format = IntFormat.newInstance(8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_FLOAT);
        int duration = 100;
        int bufferSizeInBytes = format.getBufferSizeInBytes(duration) + getMinBufferSize(format.sampleRate, format.channelMask, format.encoding);
        int periodInFrames = format.getPeriodInFrames(duration);
        int bufferSize = format.getBufferSize(duration);
        ret = new IntMeter(MediaRecorder.AudioSource.MIC, format.sampleRate, format.channelMask, format.encoding, bufferSizeInBytes);
        ret.setPositionNotificationPeriod(periodInFrames);
        ret.setRecordPositionUpdateListener(ret);
        ret.listeners = new Listeners();
        ret.audioData = new float[bufferSize];
        return ret;
    }
}
