package application.deintercom;

import java.io.IOException;
import java.util.ArrayList;
import library.deintercom.IntClient;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class IntDownloader extends LjlObject implements IntClient.DownloadAsyncCallback {
    public interface Listener {
        void file(IntDownloader sender);
        void exception(IntDownloader sender, GeException exception);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void file(IntDownloader sender) {
            for (Listener listener : this) {
                listener.file(sender);
            }
        }

        @Override
        public void exception(IntDownloader sender, GeException exception) {
            for (Listener listener : this) {
                listener.exception(sender, exception);
            }
        }
    }

    private static IntDownloader ret;
    public Listeners listeners;
    public String file;

    @Override
    public void callback(GeException exception) {
        try {
            if ((exception != null) && (exception.code == 1)) return;
            String path = file;
            file = null;
            listeners.file(this);
            if (exception != null) listeners.exception(this, exception);
            if (exception != null) return;
            IntPlayer.shared().reset();
            IntPlayer.shared().setDataSource(path);
            IntPlayer.shared().prepare();
            IntPlayer.shared().start();
        } catch (IOException exception1) {
            exception1.printStackTrace();
        }
    }

    public void download(String file) {
        if (IntContext.shared().pinger == null) return;
        if (file.equals(this.file)) return;
        String[] array = file.split("/");
        if (array.length < 2) return;
        String name = array[array.length - 2] + "/" + array[array.length - 1];
        IntContext.shared().pinger.current.cancel(hashCode());
        IntContext.shared().pinger.current.downloadAsync(name, file, hashCode(), this);
        this.file = file;
        listeners.file(this);
    }

    public void abort() {
        if (IntContext.shared().pinger == null) return;
        if (file == null) return;
        IntContext.shared().pinger.current.cancel(hashCode());
        file = null;
        listeners.file(this);
    }

    public static IntDownloader shared() {
        if (ret != null) return ret;
        ret = new IntDownloader();
        ret.listeners = new Listeners();
        return ret;
    }
}
